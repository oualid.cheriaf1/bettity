import React, { useContext, useState } from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";

import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import { AuthContext } from "../../contexts/AuthContext";
import { LoginDialog } from "../LoginDialog/LoginDialog";
import { NavDrawer } from "../NavDrawer/NavDrawer";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      flexGrow: 1
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      flexGrow: 1
    }
  })
);

export const Navbar = () => {
  const [openDialog, setOpenDialog] = useState(false);
  const [openDrawer, setOpenDrawer] = useState(false);
  const { authState, logoutAction } = useContext(AuthContext);
  const classes = useStyles();
  return (
    <AppBar position="static" color="primary" elevation={0}>
      <Toolbar variant="dense">
        <IconButton
          edge="start"
          className={classes.menuButton}
          color="inherit"
          aria-label="menu"
          onClick={() => setOpenDrawer(true)}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" className={classes.title}>
          Bettity
        </Typography>
        {!authState.isConnected && (
          <>
            <Button color="inherit" onClick={() => setOpenDialog(true)}>
              Login
            </Button>
            <LoginDialog open={openDialog} setOpen={setOpenDialog} />
          </>
        )}
        {authState.isConnected && (
          <Button color="inherit" onClick={logoutAction}>
            Logout
          </Button>
        )}
        <NavDrawer
          open={openDrawer}
          close={() => setOpenDrawer(false)}
        />
      </Toolbar>
    </AppBar>
  );
};
