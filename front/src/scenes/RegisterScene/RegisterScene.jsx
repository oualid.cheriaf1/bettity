import React, { useContext, useState } from "react";
import { Field, Form, Formik } from "formik";
import { TextField } from "formik-material-ui";
import ReCAPTCHA from "react-google-recaptcha";
import { Redirect } from "react-router-dom";
import * as Yup from "yup";

import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";

import { PageContent } from "../../components/PageContent/PageContent";
import { PageTitle } from "../../components/PageTitle/PageTitle";
import { AuthContext } from "../../contexts/AuthContext";
import { SnackbarContext } from "../../contexts/SnackbarContext";
import { useApi } from "../../core/hooks/useApi";

export const RegisterScene = () => {
  const { authState } = useContext(AuthContext);
  const { open: openSnackbar } = useContext(SnackbarContext);
  const API = useApi();
  const [googleReCaptchaValue, setGoogleReCaptchaValue] = useState();

  const sendRegisterRequest = async (values, setSubmitting) => {
    try {
      const json = await API({
        url: "/auth/register",
        method: "POST",
        data: { ...values, googleReCaptchaValue }
      });

      openSnackbar({
        severity: "success",
        message: json.message
      });
    } catch (error) {
      openSnackbar({
        severity: "error",
        message: error.message
      });
    }
    setSubmitting(false);
  };
  return authState.isConnected ? (
    <Redirect to="/"></Redirect>
  ) : (
    <>
      <PageTitle title="Inscription" />
      <PageContent>
        <Paper elevation={0}>
          <Box p={2}>
            <Formik
              initialValues={{
                email: "",
                password: "",
                confirmPassword: "",
                username: ""
              }}
              validationSchema={Yup.object({
                email: Yup.string().email(
                  "Veuillez entrer une adresse mail valide"
                ),
                password: Yup.string().min(
                  8,
                  "Le mot de passe doit contenir au minimum 8 caractères"
                ),
                username: Yup.string(),
                confirmPassword: Yup.string().oneOf(
                  [Yup.ref("password"), null],
                  "Les mots de passes doivent être identiques"
                )
              })}
              onSubmit={(values, { setSubmitting }) =>
                sendRegisterRequest(values, setSubmitting)
              }
            >
              {({ isSubmitting, isValid, dirty }) => (
                <>
                  <Form>
                    <Box mb={2}>
                      <Field
                        name="email"
                        label="Entrez votre adresse mail"
                        type="email"
                        component={TextField}
                        fullWidth
                      ></Field>
                    </Box>
                    <Box my={2}>
                      <Field
                        name="username"
                        label="Entrez votre surnom"
                        type="text"
                        component={TextField}
                        fullWidth
                      ></Field>
                    </Box>
                    <Box my={2}>
                      <Field
                        name="password"
                        label="Entrez votre mot de passe"
                        type="password"
                        component={TextField}
                        fullWidth
                      ></Field>
                    </Box>
                    <Box my={2}>
                      <Field
                        name="confirmPassword"
                        label="Confirmez votre mot de passe"
                        type="password"
                        component={TextField}
                        fullWidth
                      ></Field>
                    </Box>
                    <Box display="flex" justifyContent="center" pb={1}>
                      <ReCAPTCHA
                        sitekey={process.env.REACT_APP_RECAPTCHA_CLIENT_KEY}
                        theme="dark"
                        onChange={value => setGoogleReCaptchaValue(value)}
                      ></ReCAPTCHA>
                    </Box>
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      disableElevation
                      color="primary"
                      disabled={
                        !googleReCaptchaValue ||
                        isSubmitting ||
                        !isValid ||
                        !dirty
                      }
                    >
                      S'inscrire
                    </Button>
                  </Form>
                </>
              )}
            </Formik>
          </Box>
        </Paper>
      </PageContent>
    </>
  );
};
