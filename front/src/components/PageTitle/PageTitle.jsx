import React from "react";

import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

export const PageTitle = ({ title }) => {
  return (
    <Box textAlign="center" pt={2}>
      <Typography variant="h3">{title}</Typography>
    </Box>
  );
};
