from test.unit import BaseTest
from src.app.models.user import User, UserRole
from src.app import db


class UserModelTest(BaseTest):

    def test_should_crypt_password(self):
        """ Should encrypt the password during object initialisation """
        user = User(username='fakeUser', email='fake@fake_provider.com',
                    password="very_long_password")
        self.assertTrue(user.password_hash != "very_long_password")

    def test_encode_token(self):
        """ User object should have a function to encode the auth_token. """
        user = User(username='fakeUser', email='fake@fake_provider.com',
                    password="very_long_password")
        # pylint: disable=maybe-no-member
        db.session.add(user)
        # pylint: disable=maybe-no-member
        db.session.commit()
        auth_token = user.encode_auth_token()
        self.assertTrue(isinstance(auth_token, bytes))

    def test_decode_token(self):
        """User class should have a static method to decode auth token"""
        user = User(username='fakeUser', email='fake@fake_provider.com',
            password="very_long_password")
        # pylint: disable=maybe-no-member
        db.session.add(user)
        # pylint: disable=maybe-no-member
        db.session.commit()
        auth_token = user.encode_auth_token()
        self.assertTrue(isinstance(auth_token, bytes))
        decoded_token = User.decode_auth_token(auth_token.decode('utf-8'))
        self.assertTrue(decoded_token["sig"] == 1)
