import React, { useEffect, useState } from "react";

import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

import AddIcon from "@material-ui/icons/Add";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";

import { Link } from "../../../components/Link/Link";
import { useApi } from "../../../core/hooks/useApi";
import { CreateLeagueForm } from "./CreateLeagueForm";
import { JoinLeagueForm } from "./JoinLeagueForm";

export const LeagueListScene = () => {
  const API = useApi();
  const [leagues, setLeagues] = useState();
  const [openCreateLeagueForm, setOpenCreateLeagueForm] = useState(false);
  const [openJoinLeagueForm, setOpenJoinLeagueForm] = useState(false);
  const fetch = async () => {
    const json = await API({ url: "/leagues/", method: "GET" });
    setLeagues(json.data.leagues_list);
  };
  useEffect(() => {
    fetch();
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <Box m={2}>
        {leagues && leagues.length === 0 && (
          <Box bgcolor="error.main" p={1} borderRadius={3}>
            <Typography variant="h6">Pas de ligues</Typography>
          </Box>
        )}
        {leagues &&
          leagues.map(league => (
            <Box m={1} key={league.league_code}>
              <Paper>
                <Box p={2}>
                  <Typography variant="h5">
                    <Link
                      to={`/user/leagues/${league.league_code}`}
                      color="inherit"
                    >
                      {league.league_name}
                    </Link>
                  </Typography>
                </Box>
              </Paper>
            </Box>
          ))}
      </Box>
      <ButtonGroup>
        <Button
          color="default"
          aria-label="add"
          onClick={() => setOpenJoinLeagueForm(true)}
          variant="contained"
          endIcon={<ArrowForwardIosIcon />}
        >
          Rejoindre une ligue
        </Button>
        <Button
          color="primary"
          aria-label="add"
          onClick={() => setOpenCreateLeagueForm(true)}
          variant="contained"
          endIcon={<AddIcon />}
        >
          Créer une ligue
        </Button>
      </ButtonGroup>
      <CreateLeagueForm
        open={openCreateLeagueForm}
        onClose={() => setOpenCreateLeagueForm(false)}
        refetch={fetch}
      ></CreateLeagueForm>
      <JoinLeagueForm
        open={openJoinLeagueForm}
        onClose={() => setOpenJoinLeagueForm(false)}
        refetch={fetch}
      ></JoinLeagueForm>
    </>
  );
};
