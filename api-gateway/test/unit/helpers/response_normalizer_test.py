import unittest
from src.helpers.response_normalizer import response_normalizer


class ResponseNormalizer(unittest.TestCase):
    def test_should_return_the_right_response(self):
        """ Should return the right response. """
        response, status_code = response_normalizer("SERVER_ON")
        self.assertEqual(status_code, 200)
        self.assertEqual(response["status"], 200)
        self.assertEqual(response["message"], "Le serveur fonctionne")
        self.assertDictEqual({
            "status": 200,
            "message": "Le serveur fonctionne"
        }, response)

    def test_should_return_the_right_response_with_data(self):
        """ Should return the right response with data """
        response, status_code = response_normalizer(
            "SERVER_ON", data={"argument1": "value1"})
        self.assertEqual(status_code, 200)
        self.assertEqual(response["status"], 200)
        self.assertEqual(response["message"], "Le serveur fonctionne")
        self.assertEqual(response["data"], {"argument1": "value1"})
        self.assertDictEqual({
            "status": 200,
            "message": "Le serveur fonctionne",
            "data": {"argument1": "value1"}
        }, response)


if __name__ == '__main__':
    unittest.main()
