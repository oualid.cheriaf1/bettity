import enum
import jwt
from datetime import datetime, timedelta
from src.app import db, app, bcrypt
from sentry_sdk import capture_exception

secret_key = app.config["SECRET_KEY"]


class UserRole(enum.Enum):
    user = 1
    admin = 2


class User(db.Model):
    """
        User model of the database
    """
    # pylint: disable=maybe-no-member
    id = db.Column(
        db.Integer, primary_key=True)
    # pylint: disable=maybe-no-member
    username = db.Column(db.String(64), index=True,
                         unique=True)
    # pylint: disable=maybe-no-member
    email = db.Column(db.String(120), index=True,
                      unique=True)
    # pylint: disable=maybe-no-member
    password_hash = db.Column(
        db.String(128))
    # pylint: disable=maybe-no-member
    role = db.Column(
        db.Enum(UserRole))

    def __init__(self, username, email, password, role=UserRole.user):
        """ 
            Constructor which set the default fields and hash the password.
        """
        self.username = username
        self.email = email
        self.password_hash = bcrypt.generate_password_hash(password).decode()
        self.role = role

    def encode_auth_token(self):
        """
            Generate auth token

            :return: The authorization token
            :rtype: string
        """
        try:
            payload = {
                "exp": datetime.utcnow()+timedelta(days=30),
                "iat": datetime.utcnow(),
                "sig": self.id
            }
            return jwt.encode(payload, secret_key, "HS256")
        except Exception as e:
            capture_exception(e)
            return e

    @staticmethod
    def decode_auth_token(token):
        """
            Decode the auth token

            :param token: The auth token to decode
            :type token: string

            :return: The user id
            :rtype: string|number

            :raises ExpiredSignatureError: If the token is too old
            :raises InvalidTokenError: If the token is invalid
        """
        try:
            return jwt.decode(token, secret_key, algorithms="HS256")
        except jwt.ExpiredSignatureError:
            capture_exception()
            raise Exception("EXPIRED_TOKEN")
        except jwt.InvalidTokenError:
            capture_exception()
            raise Exception("INVALID_TOKEN")

    def __repr__(self):
        return '<User {}>'.format(self.username)
