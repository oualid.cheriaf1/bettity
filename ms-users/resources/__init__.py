from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


import os

Base = declarative_base()
engine = create_engine(os.getenv("DATABASE_URL"), echo=True)
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()