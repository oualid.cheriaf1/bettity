import http.client
import json
from interface.interface import RPCServer
import os
import pymongo
import copy
import requests


class EuroLeagueService(RPCServer):
    """ 
        The class:`EuroLeagueService` manage all the informations we retrieve from the football-data api. 
        It uses MongoDb and retrieve the credentials from environment variable 

        :param exchange_name: The RabbitMQ exchange name to use.
        :type exchange_name: str, required.

        :param queue_name: The RabbitMQ queue name to use.
        :type queue_name: str, required.
    """

    def __init__(self, exchange_name, queue_name):
        """Initialize the EuroLeagueService and connect to MongoDB."""
        super().__init__(exchange_name=exchange_name, queue_name=queue_name)
        self.API_FOOTBALL_DATA = os.getenv("FOOTBALL_DATA_URL")
        MONGO_DB_URI = os.getenv("MONGO_DB_URI")
        self.db = pymongo.MongoClient(
            MONGO_DB_URI)["heroku_j4wtgbxw"]

    def _build_request(self, path):
        """ Build and send a HTTP request to api football data. 
            :param path: The url path of the requested resource
            :type path: str, required.

            :return: Body of the HTTP response.
            :rtype: dict
        """
        headers = {'X-Auth-Token': os.getenv("FOOTBALL_DATA_API")}
        return requests.get(self.API_FOOTBALL_DATA + path, headers=headers).json()

    def get_competition_details(self):
        """ Get all the current competitions details.
            :return: Competition details with the standings
            :rtype: dict
        """
        path = "/v2/competitions/2018/standings"
        response = self._build_request(path)
        return response

    def get_competition_teams(self):
        """ Get all the teams registred in the competitions and save it in the Mongo database. 
            :return: Competitions details with all registred teams 
            :rtype: dict
        """
        path = "/v2/competitions/2018/teams"
        response = self._build_request(path)
        teamsCollection = self.db.teams
        for teams in response["teams"]:
            teamsCollection.find_one_and_update(
                copy.deepcopy(teams), {"$set": copy.deepcopy(teams)}, upsert=True)
        return response

    def get_competition_matches(self):
        """ Get all the matches registred in the competitions and save it in the Mongo database. 
            :return: Competitions details with all matches
            :rtype: dict
        """
        path = "/v2/competitions/2018/matches"
        response = self._build_request(path)
        matchesCollection = self.db.matches
        for match in response["matches"]:
            matchesCollection.find_one_and_update(
                {"id": match["id"]}, {"$set": copy.deepcopy(match)}, upsert=True)
        return response
