from flask import Flask
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

from src.config.config import DevConfig
from src.helpers.error_handler import ErrorHandler
from src.helpers.response_normalizer import response_normalizer

from src.helpers.RPCClient import RPCClient


from flask_caching import Cache
                      

# App initialization and configuration
app = Flask(__name__)
CORS(app)
app.config.from_object(DevConfig)
db = SQLAlchemy(app=app)
cache = Cache(app=app, config={
              "CACHE_KEY_PREFIX": "euro_league", "CACHE_TYPE": "filesystem", "CACHE_DIR": "./cache", "CACHE_DEFAULT_TIMEOUT":50	})
bcrypt = Bcrypt(app=app)


@app.route('/')
@ErrorHandler
def server_status():
    """
        Return a success response if the server is on.
    """
    return response_normalizer("SERVER_ON")
    
from src.app.controller.auth_controller import auth_controller
from src.app.controller.user_controller import user_controller
from src.app.controller.euro_league_controller import euro_league_controller
from src.app.controller.bet_controller import bet_controller
from src.app.controller.friend_leagues_controller import friend_leagues_controller
app.register_blueprint(auth_controller)
app.register_blueprint(user_controller)
app.register_blueprint(euro_league_controller)
app.register_blueprint(bet_controller)
app.register_blueprint(friend_leagues_controller)
