import React, { createContext, useState } from "react";
import Box from "@material-ui/core/Box";
import Snackbar from "@material-ui/core/Snackbar";
import Typography from "@material-ui/core/Typography";

export const SnackbarContext = createContext({
  open: () => {},
  close: () => {}
});

const Alert = ({ message, severity }) => {
  return (
    <Box
      p={1}
      bgcolor={`${severity}.main`}
      borderRadius="10px"
      px={3}
      margin={5}
      width="80vw"
      color="white"
      textAlign="center"
    >
      <Typography>{message}</Typography>
    </Box>
  );
};

export const SnackbarContextProvider = ({ children }) => {
  const [isOpen, setIsOpen] = useState(false);
  const open = alertProps => {
    setAlertProps(alertProps);
    setIsOpen(true);
  };
  const close = () => setIsOpen(false);
  const [alertProps, setAlertProps] = useState({
    message: "Hello World",
    severity: "error"
  });
  return (
    <>
      <SnackbarContext.Provider value={{ open, close }}>
        {children}
      </SnackbarContext.Provider>
      <Snackbar open={isOpen} autoHideDuration={2000} onClose={close}>
        <Alert {...alertProps}></Alert>
      </Snackbar>
    </>
  );
};
