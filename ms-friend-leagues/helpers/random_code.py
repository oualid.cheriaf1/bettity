from random import randint

ALPHABET = "1234567890AZERTYUIOPQSDFGHJKLMWXCVBN"
len_alphabet = len(ALPHABET)

def gen_random_code(code_length=6):
    """ Generate a random code using ALPHABET. 

        :param code_length: the length of the code to generate. Optional, default to 6.
        :type code_length: int
        :return: A code containing `code_length` characters.
        :rtype: string.
    """
    code = ""
    for _ in range(code_length):
        code += ALPHABET[randint(0, len_alphabet-1)]
    return code
