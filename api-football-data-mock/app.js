var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const v2Router = require("./routes")
const cors = require("cors")
var app = express();
app.use((req, res, next)=>{
    console.log("request arrived")
    next()
    })
app.use(cors())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use("/v2", v2Router)

module.exports = app;
