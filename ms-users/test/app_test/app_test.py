import unittest
from interface.RPCServer import RPCServer
from app.app import UserService
from unittest.mock import patch
import os
class UserServiceTest(unittest.TestCase):
    @patch("pika.BlockingConnection")
    @patch("pika.URLParameters")
    def setUp(self, URLParametersMock, BlockingConnectionMock, sessionMock):
        self.user_service = UserService("fake_user_service", "fake_user_service-rk",
                                        pika_connection_parameters=[URLParametersMock(os.getenv("RABBITMQ_URI"))])
    