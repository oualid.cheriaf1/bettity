from flask import Blueprint, request, g

from src.helpers.error_handler import ErrorHandler
from src.helpers.response_normalizer import response_normalizer
from src.helpers.validators import is_defined_in_dict

from src.app.middlewares.authenticated_as import authenticated_as

from src.app.models.user import UserRole
from src.app.microservices.bet_service import BetService
bet_controller = Blueprint("bet_controller", __name__, url_prefix="/bets")


@bet_controller.route("/", methods=["GET"])
@ErrorHandler
@authenticated_as(UserRole.user)
def get_all_bets():
    """ Route to get all bets of the connected user. """
    bet_list = BetService().get_all_bets_by_user_id(g.user.id)
    return response_normalizer("BETS_RETRIEVED", {"bets_list": bet_list})

@bet_controller.route('/', methods=["POST"])
@ErrorHandler
@authenticated_as(UserRole.user)
def save_bets():
    """ Route to create or update the bets. """
    user = g.user
    bet_list = request.get_json()
    # TODO: Verify the match date using the EuroLeagueService
    response = BetService().save_bets(user_id=user.id, bet_list=bet_list)
    return response_normalizer("BET_SAVED", response)
