import unittest
from resources import Base
from resources.user import User, UserRole


class UserTest(unittest.TestCase):
    """ Test of the User Resource """

    def test_user(self):
        """ Should init a user """
        self.assertTrue(issubclass(User, Base))
        fake_user = User("ocheriaf", "ocheriaf@gmail.com",
                         "password_hash", role=UserRole.admin)
        self.assertEqual(fake_user.email, "ocheriaf@gmail.com")
        self.assertEqual(fake_user.username, 'ocheriaf')
        self.assertEqual(fake_user.password_hash, "password_hash")
        self.assertEqual(fake_user.role, UserRole.admin)

    def test_user_to_dict(self):
        """ Should export user to dict """
        fake_user = User("ocheriaf", "ocheriaf@gmail.com",
                         "password_hash", role=UserRole.admin)
        user_dict = fake_user.to_dict()
        self.assertDictEqual({"email": "ocheriaf@gmail.com", "username": "ocheriaf",
                              "id": None, "role": UserRole.admin.value}, user_dict)
        self.assertNotIn("password_hash", user_dict)

    def test_repr_user(self):
        fake_user = User("ocheriaf", "ocheriaf@gmail.com",
                         "password_hash", role=UserRole.admin)
        self.assertEqual(fake_user.__repr__(), "<User ocheriaf>")
