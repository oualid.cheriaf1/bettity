import sentry_sdk
import click
from app.app import FriendLeagueService
from dotenv import load_dotenv
import os
load_dotenv(verbose=True)
sentry_sdk.init(os.getenv('SENTRY_DSN'))


@click.group()
def cli():
    pass


@cli.command()
def run():
    friend_league_service = FriendLeagueService(
        "friend_league_service", "friend_league_service-rk")
    friend_league_service.listen()


@cli.command()
def test():
    import unittest
    tests = unittest.TestLoader().discover('.', pattern="*_test.py")
    results = unittest.TextTestRunner(verbosity=2).run(tests)
    if results.wasSuccessful():
        return 0
    return 1


if __name__ == "__main__":
    cli()
