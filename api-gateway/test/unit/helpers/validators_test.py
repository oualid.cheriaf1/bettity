import unittest

from src.helpers.validators import is_defined_in_dict

class ValidatorsTest(unittest.TestCase):
    def test_should_is_defined_in_dict(self):
        """ Method should return a boolean if key is defined in dict, if not, should raise an Exception with args = ('INVALID_DATA') """
        dict_fixture = {"Hello":"World", 3:"Bonjour", "Coucou":"toi"}
        self.assertTrue(is_defined_in_dict("Hello", dict_fixture))
        self.assertTrue(is_defined_in_dict(3, dict_fixture))
        self.assertTrue(is_defined_in_dict("Coucou", dict_fixture))
        self.assertTrue(is_defined_in_dict(["Hello", "Coucou"], dict_fixture))
        self.assertTrue(is_defined_in_dict(["Coucou", 3], dict_fixture))
        with self.assertRaises(Exception) as cm:
            is_defined_in_dict('undefined_key', dict_fixture)
            exception = cm.exception
            self.assertTrue(exception.args == ("INVALID_DATA"))
        with self.assertRaises(Exception) as cm:
            is_defined_in_dict("3", dict_fixture)
            exception = cm.exception
            self.assertTrue(exception.args == ("INVALID_DATA"))