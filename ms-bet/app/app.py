from interface.interface import RPCServer
import os
import pymongo
import pika 
from dateutil.parser import isoparse
from datetime import datetime
from dateutil.tz import tzutc
class BetService(RPCServer):
    def __init__(self, pika_connection_parameters=[pika.URLParameters(os.getenv("RABBITMQ_URI"))]):
        """ Initialize the BetService and connect to the MongoDB """
        super().__init__("bet_service", "bet_service-rk", pika_connection_parameters=pika_connection_parameters)
        MONGO_URI = os.getenv("MONGO_DB_URI")
        self.db = pymongo.MongoClient(MONGO_URI)["heroku_j4wtgbxw"]

    def save_bets(self, user_id, bet_list):
        """ Update or save the bets for a specific user into the Mongo database. 

            :param user_id: The user identifier
            :type user_id: int, required.

            :param bet_list: List of bet (A dict object)
            :type bet_list: List of dict, required.

            :return: True if success
            :rtype: Boolean

            :raises Exception: Raise an exception if an error occured
        """
        bets_collection = self.db.bets_collection
        now = datetime.utcnow().replace(tzinfo=tzutc())
        for bet in bet_list:
            match_datetime = isoparse(bet["matchUtcDate"])
            if now < match_datetime:
                bets_collection.find_one_and_update(
                    {"user_id": user_id, "match_id": bet["matchId"]}, {"$set": {"user_id": user_id, **bet}}, upsert=True)
            else: 
                raise Exception("INVALID_DATA")
        return True

    def get_all_bets_by_user_id(self, user_id):
        """ Get all the user bets. 
        
            :param user_id: The user identifier
            :type user_id: int, required.

            :return: The list of user bets 
            :rtype: List of dict
        """
        bets_collection = self.db.bets_collection
        bets_with_objectId_list = list(
            bets_collection.find({"user_id": user_id}))
        bets_list = [{"matchId": u['matchId'], "scoreAway": u["scoreAway"], "scoreHome":u["scoreHome"]}
                     for u in bets_with_objectId_list]        
        return bets_list
