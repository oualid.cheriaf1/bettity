import jwt
import sqlalchemy
from src.app import db, bcrypt
from flask import Blueprint, request, g
from sentry_sdk import capture_exception
from src.app.models.user import User, UserRole
from src.helpers.response_normalizer import response_normalizer
from src.helpers.error_handler import ErrorHandler
from src.helpers.validators import is_defined_in_dict, verify_google_recaptcha
from src.app.middlewares.authenticated_as import authenticated_as
auth_controller = Blueprint("auth_controller", __name__, url_prefix="/auth")


@auth_controller.route("/register", methods=["POST"])
@ErrorHandler
def register():
    """ Route to create a new user """
    try:
        body = request.get_json()
        is_defined_in_dict(
            ["password", "email", "username", "googleReCaptchaValue"], body)

        verify_google_recaptcha(
            google_recaptcha_value=body["googleReCaptchaValue"])
            
        user = User.query.filter(sqlalchemy.or_(
            User.email == body["email"], User.username == body["username"])).first()
        if not user:
            user = User(username=body["username"],
                        email=body["email"], password=body["password"])
            # pylint: disable=maybe-no-member
            db.session.add(user)
            # pylint: disable=maybe-no-member
            db.session.commit()
            return response_normalizer("USER_CREATED")
        else:
            raise Exception("USER_ALREADY_EXISTS")
    except KeyError as e:
        capture_exception(e)
        raise Exception('INVALID_DATA', e.args)
    except sqlalchemy.exc.IntegrityError as e:
        capture_exception(e)
        raise Exception('USERNAME_ALREADY_TAKEN', e.args)


@auth_controller.route('/login', methods=["POST"])
@ErrorHandler
def login():
    """ Route to login """
    try:
        body = request.get_json()
        user = User.query.filter_by(email=body['email']).first()
        if not user:
            raise Exception("USER_NOT_FOUND")
        if not bcrypt.check_password_hash(user.password_hash, body["password"]):
            raise Exception("INVALID_PASSWORD")
        return response_normalizer("USER_LOGGED", {"token": user.encode_auth_token().decode()})
    except KeyError as e:
        capture_exception(e)
        raise Exception('INVALID_DATA', e.args)
