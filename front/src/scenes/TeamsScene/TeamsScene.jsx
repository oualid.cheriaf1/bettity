import React, { useState, useEffect } from "react";

import Avatar from "@material-ui/core/Avatar";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

import { PageContent } from "../../components/PageContent/PageContent";
import { PageTitle } from "../../components/PageTitle/PageTitle";
import { useApi } from "../../core/hooks/useApi";
import { TEAMS } from "../../core/resources/teams";

export const TeamsScene = () => {
  const API = useApi();
  const [teams, setTeams] = useState();
  const getTeams = async () => {
    const { data } = await API({ url: "/euro/teams", method: "GET" });
    setTeams(data.teams);
  };
  useEffect(() => {
    getTeams();
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {teams && teams.length > 0 && (
        <>
          <PageTitle title="Les équipes" />
          <PageContent>
            <Box my={2}>
              <Grid container justify="space-around" spacing={2}>
                {teams.map(t => {
                  return (
                    <Grid key={t.id} item xs={6}>
                      <Paper elevation={0}>
                        <Box
                          p={2}
                          justifyContent="space-around"
                          alignItems="center"
                          display="flex"
                          flexDirection="column"
                        >
                          <Avatar
                            alt={t.name}
                            src={
                              (TEAMS[t.id] && TEAMS[t.id].crestUrl) ||
                              t.crestUrl ||
                              ""
                            }
                          ></Avatar>
                          <Typography variant="h6">
                            {(TEAMS[t.id] && TEAMS[t.id].name) || t.name}
                          </Typography>
                        </Box>
                      </Paper>
                    </Grid>
                  );
                })}
              </Grid>
            </Box>
          </PageContent>
        </>
      )}
    </>
  );
};
