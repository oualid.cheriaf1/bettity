import React from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";

import { PageContent } from "../../../components/PageContent/PageContent";
import { PageTitle } from "../../../components/PageTitle/PageTitle";
import { LeagueDetailsScene } from "./LeagueDetailsScene";
import { LeagueListScene } from "./LeagueListScene";

export const LeagueScene = () => {
  const { path } = useRouteMatch();
  return (
    <>
      <PageTitle title="Mes ligues" />
      <PageContent>
        <Switch>
          <Route path={`${path}/:leagueId`}>
            <LeagueDetailsScene />
          </Route>
          <Route path={path}>
            <LeagueListScene />
          </Route>
        </Switch>
      </PageContent>
    </>
  );
};
