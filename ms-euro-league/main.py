import click
import os
from dotenv import load_dotenv
load_dotenv()
import sentry_sdk
sentry_sdk.init(os.getenv('SENTRY_DSN'))


@click.group()
def cli():
    pass


@cli.command()
def run():
    from app.app import EuroLeagueService
    euro_league_service = EuroLeagueService(
        "euro_league_service", "euro_league_service-rk")
    euro_league_service.listen()


@cli.command()
def test():
    import unittest
    tests = unittest.TestLoader().discover(".", pattern='*_test.py')
    results = unittest.TextTestRunner(verbosity=2).run(tests)
    if results.wasSuccessful():
        return 0
    return 1


if __name__ == "__main__":
    cli()
