import os
import sentry_sdk
from dotenv import load_dotenv
load_dotenv(verbose=True)
from src.app import app, db
from flask_script import Manager
from flask_migrate import MigrateCommand, Migrate
import unittest
sentry_sdk.init(os.getenv('SENTRY_DSN'))

manager = Manager(app=app)
migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)


@manager.command
def unit_test():
    """Runs the unit tests"""
    tests = unittest.TestLoader().discover('test/unit', pattern='*_test.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1


@manager.command
def dev():
    """Run the flask application in debug mode"""
    db.create_all()
    app.run(host="0.0.0.0",debug=True)



@manager.command
def create_db():
    """Creates the db tables."""
    db.create_all()


@manager.command
def drop_db():
    """Drop the database"""
    db.drop_all()

@manager.command
def run_app():
    """Run the app"""
    app.run(host="0.0.0.0", port=os.getenv("PORT"))

if __name__ == "__main__":
    manager.run()
