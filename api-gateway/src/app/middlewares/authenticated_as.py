from flask import request,g
from src.app import db, bcrypt
from src.app.models.user import User, UserRole
from src.helpers.response_normalizer import response_normalizer

def authenticated_as(role):
    def verify_auth_state(func, *args, **kwargs):
        def verifier(*args, **kwargs):
            auth_header = request.headers.get('Authorization')
            if auth_header:
                try:
                    auth_token = auth_header.split(" ")[1]
                except IndexError:
                    return response_normalizer("FORBIDDEN_ACCESS")
            else:
                auth_token = ''
            if auth_token:
                resp = User.decode_auth_token(auth_token)["sig"]
                if isinstance(resp, int):
                    user = User.query.filter_by(id=resp).first()
                    if user.role.value >= role.value:
                        g.user = user
                        return func(*args, **kwargs)
                return response_normalizer("FORBIDDEN_ACCESS")
            else:
                return response_normalizer("FORBIDDEN_ACCESS")
        verifier.__name__ = func.__name__
        return verifier

    return verify_auth_state
