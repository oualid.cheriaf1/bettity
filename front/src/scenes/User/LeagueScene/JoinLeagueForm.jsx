import React, { useContext } from "react";
import { Formik, Form, Field } from "formik";
import { TextField } from "formik-material-ui";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Slide from "@material-ui/core/Slide";
import Typography from "@material-ui/core/Typography";

import { SnackbarContext } from "../../../contexts/SnackbarContext";
import { useApi } from "../../../core/hooks/useApi";

export const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
export const JoinLeagueForm = ({ open, onClose, refetch }) => {
  const API = useApi();
  const { open: openSnackbar } = useContext(SnackbarContext);
  const sendJoinRequest = async (values, { setSubmitting }) => {
    try {
      const json = await API({
        url: `/leagues/join/${values.league_code.toUpperCase()}`,
        method: "POST"
      });
      openSnackbar({ severity: "success", message: json.message });
    }
    catch (error) {
      openSnackbar({ severity: "error", message: error.message });
    }
    refetch();
    setSubmitting(false);
    onClose();
  };
  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={onClose}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogContent>
        <Formik
          initialValues={{ league_code: "" }}
          onSubmit={sendJoinRequest}
        >
          {() => (
            <Form>
              <Typography> Rejoindre la ligue</Typography>
              <Field
                name="league_code"
                label="Code de la ligue"
                type="text"
                component={TextField}
                margin="normal"
                fullWidth
              ></Field>
              <Button
                type="submit"
                color="primary"
                fullWidth
                variant="contained"
              >
                Créer
              </Button>
            </Form>
          )}
        </Formik>
      </DialogContent>
    </Dialog>
  );
};
