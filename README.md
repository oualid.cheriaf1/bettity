# BETTITY

Projet interne de Coddity

## Description

Bettity est une application de paris entre amis sur une compétition footballistique (dans notre cas, l’euro 2020). 

## Architecture

Bettity est une application conçue à l’aide de microservices et d’une api qui sert de porte d’entrée (*api-gateway*). L’ensemble des microservices est relié à l’**API** à l’aide de **RabbitMQ**. 

Le dossier *ms-template* contient un boilerplate de base pour créer un microservice (Non récemment mis à jour.)

Le dossier *ms-scrapper* contient le script de scrapping du classement fifa. 

L'application utilise l'API de [football-data](https://www.football-data.org/) afin de récupérer les données. Un mock de cette API (*api-football-data-mock*) a été produit pour des fins de développement (non testé mais permets de récupérer l'ensemble des données.)

Le front est séparé du backend et utilise React (à installer et lancer séparément à partir du dossier *front*). 

## Installation

### Requirements 

    - Docker
    - Un compte Sentry

### Environnement de développement

1 - Pour chaque microservices et front : 

- ```cp .env.example .env```
- Ouvrir chaque **.env** et remplacer les variables

2 - Pour démarrer l'api et l'ensemble des microservices et leurs dépendances : 

- Lancer docker
- ``` docker-compose up ```

3 - Démarrer le front : 

- ``` cd front-react ```
- ``` npm install ```
- ``` npm start ```

4 - **Lors du premier lancement uniquement** : 

Nous allons scrapper le site de la Fifa pour récupérer l'ensemble des points de chaque équipes. 

- ``` cd ms-scrapper ```

Dans un environnement virtuel python ou sur le python standard

- ```pip install -r requirements.txt```
- ```python main.py```

