import React, { useMemo } from "react";
import * as moment from "moment";

import Avatar from "@material-ui/core/Avatar";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

import { TEAMS } from "../../core/resources/teams";

const useStyles = makeStyles(theme =>
  createStyles({
    winner: { color: theme.palette.success.main },
    loser: { color: theme.palette.error.main },
    draw: { color: theme.palette.info.main }
  })
);

export const classFinder = (match, style) => {
  const isFinished = match.status === "FINISHED";
  const homeTeamWin = match.score.winner === "HOME_TEAM";
  const awayTeamWin = match.score.winner === "AWAY_TEAM";
  const isDrawn = match.score.winner === "DRAW";
  return {
    homeClass:
      (isFinished && (
        (homeTeamWin && style.winner) ||
        (awayTeamWin && style.loser) ||
        (isDrawn && style.draw))) ||
      "",
    awayClass:
      (isFinished && (
        (homeTeamWin && style.winner) ||
        (awayTeamWin && style.loser) ||
        (isDrawn && style.draw))) ||
      ""
  };
};
export const MatchDescription = ({ match, teamSeparator }) => {
  const style = useStyles();
  const classes = useMemo(() => classFinder(match, style), [match, style]);
  return (
    <Paper key={match.id}>
      <Box p={2} my={1}>
        <Box
          fontWeight={700}
          fontSize={16}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <Box px={2}>
            <Avatar
              src={
                (TEAMS[match.homeTeam.id] &&
                  TEAMS[match.homeTeam.id].crestUrl) ||
                match.homeTeam.crestUrl
              }
            ></Avatar>
          </Box>
          <Box
            display="flex"
            flexDirection="row"
            justifyContent="center"
            alignItems="center"
          >
            <Box className={classes.homeClass} px={1}>
              {(TEAMS[match.homeTeam.id] && TEAMS[match.homeTeam.id].name) ||
                match.homeTeam.name}
            </Box>
            {teamSeparator ? (
              teamSeparator
            ) : (
              <MatchResult
                match={match}
                homeClass={classes.homeClass}
                awayClass={classes.awayClass}
              />
            )}
            <Box px={1} className={classes.awayClass}>
              {(TEAMS[match.awayTeam.id] && TEAMS[match.awayTeam.id].name) ||
                match.awayTeam.name}
            </Box>
          </Box>
          <Box px={2}>
            <Avatar
              src={
                TEAMS[match.awayTeam.id] && TEAMS[match.awayTeam.id].crestUrl
              }
            ></Avatar>
          </Box>
        </Box>

        <Box fontSize="12" pt={1}>
          <Typography variant="body2" color="secondary">
            {moment.utc(match.utcDate).format("LLLL")}
          </Typography>
        </Box>
      </Box>
    </Paper>
  );
};

export const MatchResult = ({ match, homeClass, awayClass }) => {
  return (
    <>
      {match.status === "FINISHED" ? (
        <>
          <Box className={homeClass} px={1}>
            {match.score.fullTime.homeTeam}
          </Box>
          -
          <Box px={1} className={awayClass}>
            {match.score.fullTime.awayTeam}
          </Box>
        </>
      ) : (
        <> - </>
      )}
    </>
  );
};
