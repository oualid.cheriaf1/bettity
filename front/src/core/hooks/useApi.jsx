import { useContext } from "react";

import { AuthContext } from "../../contexts/AuthContext";

export const useApi = () => {
  // build a function and send it back
  const { authState } = useContext(AuthContext); // Retrieve the auth state from the authContext
  const headers = { "Content-Type": "application/json" }; // Set basic headers
  if (authState.token) {
    headers.Authorization = `Bearer ${authState.token}`; // If token, add authorization token
  }
  // return a function that make calls to API
  return async options => {
    const fetchOptions = { headers }; // Build the request options
    if (options.method) {
      fetchOptions.method = options.method;
    }
    if (options.data) {
      fetchOptions.body = JSON.stringify(options.data);
    }
    // Send request and wait for api response
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}${options.url}`,
      fetchOptions
    );
    if (response.ok) {
      return response.json(); // Return json object or array.
    } else {
      const jsonResponse = await response.json();
      throw new Error(jsonResponse.message);
    }
  };
};
