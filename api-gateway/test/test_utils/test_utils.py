def build_mocked_request_post(response, status_code):
    def mocked_requests_post(*args, **kwargs):
        """
            Function to mock the post request.
        """
        class MockResponse:
            def __init__(self, json_data, status_code):
                self.json_data = json_data
                self.status_code = status_code

            def json(self):
                return self.json_data

        return MockResponse(response, status_code)
    return mocked_requests_post
