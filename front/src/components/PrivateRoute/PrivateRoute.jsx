import React, { useContext } from "react";
import { Redirect, Route } from "react-router-dom";

import { AuthContext } from "../../contexts/AuthContext";

export const PrivateRoute = props => {
  const { authState } = useContext(AuthContext);
  // A private route. Use the AuthContext State and values to define if the user is logged in and can enter the private space.
  return (
    <Route {...props}>
      {authState.isConnected ? (
        <>{props.children}</>
      ) : (
        <Redirect to="/"></Redirect>
      )}
    </Route>
  );
};
