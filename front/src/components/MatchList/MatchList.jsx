import React from "react";

import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

import { STAGES } from "../../core/resources/championshipStage";
import { MatchDescription } from "../MatchDescription/MatchDescription";

export const MatchList = ({ matches, teamSeparator }) => {
  return (
    <>
      {Object.keys(matches).map(stage => (
        <Box key={stage} textAlign="center">
          <Typography variant="h5">
            {STAGES[stage] || stage}
          </Typography>
          {stage === "GROUP_STAGE" &&
            Object.keys(matches[stage]).map(groupName => (
              <Box key={groupName}>
                <Typography variant="h6">
                  {groupName.replace("Group", "Groupe")}
                </Typography>
                {matches[stage][groupName].map(match => (
                  <MatchDescription
                    match={match}
                    key={match.id}
                    teamSeparator={teamSeparator && teamSeparator(match)}
                  />
                ))}
              </Box>
            ))}
        </Box>
      ))}
    </>
  );
};
