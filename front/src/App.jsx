import React from "react";
import "./App.scss";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import {
  red as primary,
  lightBlue as secondary
} from "@material-ui/core/colors";
import { AppContent } from "./scenes/AppContent";
import { BrowserRouter as Router } from "react-router-dom";
import { AuthContextProvider } from "./contexts/AuthContext";
import { SnackbarContextProvider } from "./contexts/SnackbarContext";

const appTheme = createMuiTheme({
  palette: {
    primary: { light: primary[700], main: primary[800], dark: primary[900] },
    secondary: {
      light: secondary.A100,
      main: secondary.A200,
      dark: secondary.A400
    },
    type: "dark"
  }
});

const App = () => {
  return (
    <SnackbarContextProvider>
      <AuthContextProvider>
        <ThemeProvider theme={appTheme}>
          <Router>
            <AppContent></AppContent>
          </Router>
        </ThemeProvider>
      </AuthContextProvider>
    </SnackbarContextProvider>
  );
};

export default App;
