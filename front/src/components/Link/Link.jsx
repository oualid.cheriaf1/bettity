import React from "react";
import { Link as RRDLink } from "react-router-dom";

import MUILink from "@material-ui/core/Link";

export const Link = props => {
  return <MUILink {...props} component={RRDLink}></MUILink>;
};
