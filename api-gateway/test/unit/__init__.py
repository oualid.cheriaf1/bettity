from flask_testing import TestCase
from src.config.config import TestConfig
from src.app import app, db
import json
from unittest.mock import patch

class BaseTest(TestCase):
    def create_app(self):
        app.config.from_object(TestConfig)
        return app

    def setUp(self):
        """Create the database before each test"""
        db.create_all()
        # pylint: disable=maybe-no-member
        db.session.commit()

    def tearDown(self):
        """Drop the database after each test"""
        db.session.remove()
        db.drop_all()

    def register_user(self, body):
        """ 
            Should send a request with a body to register a user

            :param body: The body of the post request to send
            :type body: any
        """
        return self.client.post('/auth/register', data=json.dumps({**body, "googleReCaptchaValue": "Some_used_token"}), content_type="application/json")

    def login_user(self, body):
        """ 
            Should send a request with a body to login a user

            :param body: The body of the post request to send
            :type body: any
        """
        return self.client.post('/auth/login', data=json.dumps(body), content_type="application/json")
