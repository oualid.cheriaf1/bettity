from helpers.mailer import Mailer
import os
pymailer = Mailer(os.getenv("MAILER_AUTH_USER"),
                  os.getenv("MAILER_AUTH_PASSWORD"), host=os.getenv("MAILER_HOST"), port=os.getenv("MAILER_PORT"))
