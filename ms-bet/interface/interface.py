import pika
import dill
import os
from sentry_sdk import capture_exception

class RPCServer:
    def __init__(self, exchange_name="", queue_name="", pika_connection_parameters=[pika.URLParameters(os.getenv("RABBITMQ_URI"))]):
        self.queue_name = queue_name
        self.exchange_name = exchange_name
        self.pika_connection_parameters = pika_connection_parameters
        self.connection = pika.BlockingConnection(
            self.pika_connection_parameters)
        self.channel = self.connection.channel()
        self.exchange = self.channel.exchange_declare(exchange_name)
        self.queue = self.channel.queue_declare(queue_name)
        self.channel.queue_bind(queue_name, exchange_name)

    def listen(self):
        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(
            queue=self.queue_name, on_message_callback=self.on_request)
        print("[x] Awaiting RPC requests")
        self.channel.start_consuming()

    def on_request(self, ch, method, props, body):
        try:
            args_and_kwargs = dill.loads(body)
            method_to_call = getattr(self, props.headers["method"])
            try:
                response = method_to_call(*args_and_kwargs["args"], **args_and_kwargs["kwargs"])
                ch.basic_publish(exchange='',
                            routing_key=props.reply_to,
                            properties=pika.BasicProperties(
                                correlation_id=props.correlation_id),
                            body=dill.dumps(response))
            except Exception as e:
                capture_exception(e)
                ch.basic_publish(exchange='',
                                 routing_key=props.reply_to,
                                 properties=pika.BasicProperties(
                                     correlation_id=props.correlation_id, headers={"error": e.args[0]}),
                                 body=dill.dumps(e))
            
        except AttributeError as e:
            capture_exception(e)
            ch.basic_publish(exchange='',
                             routing_key=props.reply_to,
                             properties=pika.BasicProperties(
                                 correlation_id=props.correlation_id, headers={"error":"METHOD_DOES_NOT_EXIST"}),
                             body=dill.dumps(Exception("METHOD_DOES_NOT_EXIST", props.headers["method"])))
        ch.basic_ack(delivery_tag=method.delivery_tag)
    def register(self, func):
        setattr(self, func.__name__, func)
