import React, { useContext } from "react";
import * as moment from "moment";

import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import { MatchList } from "../../../components/MatchList/MatchList";
import { PageContent } from "../../../components/PageContent/PageContent";
import { PageTitle } from "../../../components/PageTitle/PageTitle";
import { SnackbarContext } from "../../../contexts/SnackbarContext";
import { useBets } from "../../../core/hooks/api/useBets";
import { useMatches } from "../../../core/hooks/api/matches";
import { useApi } from "../../../core/hooks/useApi";

export const BetScene = () => {
  const API = useApi();
  const matches = useMatches();
  const { open } = useContext(SnackbarContext);
  const { bets, setBets } = useBets(matches);
  const sendBets = async bets => {
    const json = await API({ url: "/bets/", method: "POST", data: bets });
    open({ severity: "success", message: json.message });
  };
  return (
    <>
      <PageTitle title="Vos Paris" />
      {matches && (
        <PageContent>
          <MatchList
            matches={matches}
            teamSeparator={match =>
              moment.utc(match.utcDate).isAfter(moment.utc()) && (
                <Box display="flex" flexDirection="row">
                  <Box width={20} pr={1} textAlign="center">
                    <TextField
                      inputProps={{ min: 0 }}
                      type="number"
                      value={
                        (bets && bets[match.id] && bets[match.id].scoreHome) ||
                        0
                      }
                      onChange={ev => {
                        bets &&
                          setBets({
                            ...bets,
                            [match.id]: {
                              ...bets[match.id],
                              scoreHome: parseInt(ev.target.value, 10)
                            }
                          });
                      }}
                    ></TextField>
                  </Box>
                  -
                  <Box width={20} pl={1}>
                    <TextField
                      type="number"
                      inputProps={{ min: 0 }}
                      value={
                        (bets && bets[match.id] && bets[match.id].scoreAway) ||
                        0
                      }
                      onChange={ev => {
                        bets &&
                          setBets({
                            ...bets,
                            [match.id]: {
                              ...bets[match.id],
                              scoreAway: parseInt(ev.target.value, 10)
                            }
                          });
                      }}
                    ></TextField>
                  </Box>
                </Box>
              )
            }
          ></MatchList>
          <Button
            color="primary"
            variant="contained"
            fullWidth
            onClick={() => {
              const formattedBets = Object.keys(bets).map(
                matchId => bets[parseInt(matchId, 10)]
              );
              sendBets(formattedBets);
            }}
          >
            Sauvegarder
          </Button>
        </PageContent>
      )}
    </>
  );
};
