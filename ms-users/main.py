import click
import unittest
import os
from dotenv import load_dotenv
load_dotenv(verbose=True)
from app.app import UserService
import sentry_sdk
sentry_sdk.init(os.getenv('SENTRY_DSN'))
@click.group()
def cli():
    pass

@cli.command()
def run():
    user_service = UserService("user_service", "user_service-rk")
    user_service.listen()

@cli.command()
def test():
    tests = unittest.TestLoader().discover(".", pattern='*_test.py')
    results =  unittest.TextTestRunner(verbosity=2).run(tests)
    if results.wasSuccessful():
        return 0
    return 1

if __name__ == "__main__":
    cli()
