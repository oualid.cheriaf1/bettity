from src.helpers.response_normalizer import RESPONSES, response_normalizer
import traceback
from sentry_sdk import capture_exception

def ErrorHandler(func, *args, **kwargs):
    """
    Decorator which catch the exception raised in routes. 

        :param func: The function which will have its exception catched
        :type func: function

        :return: function with exception catched
        :rtype: 
    """
    def error_handler(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            capture_exception(e)
            if e.args[0] in RESPONSES.keys():
                return response_normalizer(e.args[0], e.args[1:])
            return {"status": 500, "message": "Une erreur interne est survenu. Veuillez réessayer plus tard.", "error": e.args, "stack":traceback.format_exc()}, 500
    error_handler.__name__=func.__name__
    return error_handler
