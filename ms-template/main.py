from interface.interface import RPCServer
from watchgod import run_process
import sentry_sdk
sentry_sdk.init(os.getenv('SENTRY_DSN'))
def run():
    inter = RPCServer("oualid", "oualid-rpc")

    def fact(n):
        if n == 1:
            return 1
        return n * fact(n-1)

    @inter.register
    def test1(arg1):
        # raise Exception('FAKE_ERROR', "Test")
        return [fact(u) for u in range(1, arg1+1)]
    inter.listen()

if __name__ == "__main__":
    run_process("./", run)
