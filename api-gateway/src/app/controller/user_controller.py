from flask import Blueprint, g
from src.app.microservices.user_service import UserService
from src.app.models.user import UserRole
from src.app.middlewares.authenticated_as import authenticated_as
from src.helpers.error_handler import ErrorHandler
from src.helpers.response_normalizer import response_normalizer
import pika
from sentry_sdk import capture_exception

user_controller = Blueprint('user_controller', __name__, url_prefix="/user")
@user_controller.route('/me', methods=["GET"])
@ErrorHandler
@authenticated_as(UserRole.user)
def my_profile():
    """ Route to return the user profile """
    try:
        user_profile = UserService().get_user(g.user.id)
    except pika.exceptions.AMQPChannelError as err:
        capture_exception(err)
        print("Caught a channel error: {}, stopping...".format(err))
    except pika.exceptions.AMQPConnectionError as err:
        capture_exception(err)
        print("Connection was closed, retrying...", err)
    return response_normalizer("USER_LOGGED", {"user": user_profile})
    
@user_controller.route("/<int:user_id>", methods=['GET'])
@ErrorHandler
@authenticated_as(UserRole.user)
def get_profile_by_id(user_id):
    """ Route to get other user's username. """
    username = UserService().get_username_by_id(user_id = user_id)
    return response_normalizer("USER_RETRIEVED", {"user": username})
