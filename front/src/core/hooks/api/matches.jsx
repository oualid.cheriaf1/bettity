import { useState, useEffect } from "react";

import { useApi } from "../useApi";

export const useMatches = () => {
  const API = useApi();
  const [matches, setMatches] = useState();
  useEffect(() => {
    API({ url: "/euro/matches", method: "GET" }).then(({ data }) => {
      const { matches } = data;

      // We have to group matches by stages
      // We gather match by stages, ex: GROUP, 1/16, 1/8, 1/4, semi-finals, final
      const gatherMatchesByStages = matches.reduce(
        (acc, currentValue) => {
          acc[currentValue.stage] = acc[currentValue.stage]
            ? [...acc[currentValue.stage], currentValue]
            : [currentValue];
          return acc;
        }, // Perform affection and return the accumulators.
        {}
      );

      // Inside each stage, we gather match by group
      // Ex : Inside group stage, we gather teams into right group
      //    France in group A
      //    Belgium in group B
      //    ...
      const gatherMatchByGroup = matchArray => {
        return matchArray.reduce((acc, currentValue) => {
          if (currentValue.group) {
            acc[currentValue.group] = acc[currentValue.group]
              ? [...acc[currentValue.group], currentValue]
              : [currentValue];
          }
          return acc;
        }, {});
      };

      // We build the match table

      let matchTable = { ...gatherMatchesByStages };
      for (let key in gatherMatchesByStages) {
        matchTable = {
          ...matchTable,
          [key]: gatherMatchByGroup(matchTable[key])
        };
      }

      setMatches(matchTable);
    });
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return matches;
};
