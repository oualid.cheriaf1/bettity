import React, { useContext } from "react";

import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";

import AssessmentOutlinedIcon from "@material-ui/icons/AssessmentOutlined";
import BallotOutlinedIcon from "@material-ui/icons/BallotOutlined";
import HomeIcon from "@material-ui/icons/HomeOutlined";
import OutlinedFlagRoundedIcon from "@material-ui/icons/OutlinedFlagRounded";
import PersonIcon from "@material-ui/icons/PersonOutline";
import SportsSoccerOutlinedIcon from "@material-ui/icons/SportsSoccerOutlined";

import { AuthContext } from "../../contexts/AuthContext";
import { Link } from "../Link/Link";

export const NavDrawer = ({ open, close }) => {
  return (
    <Drawer open={open} onClose={close}>
      <NavDrawerList close={close} />
    </Drawer>
  );
};

const NavDrawerList = ({ close }) => {
  const { authState } = useContext(AuthContext);

  return (
    <List>
      <Link to="/" color="inherit" onClick={close}>
        <ListItem button>
          <ListItemIcon>
            <HomeIcon fontSize="default" />
          </ListItemIcon>
          <ListItemText>Accueil</ListItemText>
        </ListItem>
      </Link>
      <Divider></Divider>
      {authState.isConnected && (
        <>
          <Link to="/user/profile" color="inherit" onClick={close}>
            <ListItem button>
              <ListItemIcon>
                <PersonIcon fontSize="default" />
              </ListItemIcon>
              <ListItemText>Profil</ListItemText>
            </ListItem>
          </Link>
          <Link to="/user/bets" color="inherit" onClick={close}>
            <ListItem button>
              <ListItemIcon>
                <BallotOutlinedIcon fontSize="default" />
              </ListItemIcon>
              <ListItemText>Paris</ListItemText>
            </ListItem>
          </Link>
          <Link to="/user/leagues" color="inherit" onClick={close}>
            <ListItem button>
              <ListItemIcon>
                <AssessmentOutlinedIcon fontSize="default" />
              </ListItemIcon>
              <ListItemText>Ligues</ListItemText>
            </ListItem>
          </Link>
        </>
      )}
      <Divider></Divider>
      <Link to="/teams" color="inherit" onClick={close}>
        <ListItem button>
          <ListItemIcon>
            <OutlinedFlagRoundedIcon fontSize="default" />
          </ListItemIcon>
          <ListItemText>Equipes</ListItemText>
        </ListItem>
      </Link>
      <Link to="/matches" color="inherit" onClick={close}>
        <ListItem button>
          <ListItemIcon>
            <SportsSoccerOutlinedIcon fontSize="default" />
          </ListItemIcon>
          <ListItemText>Résultats</ListItemText>
        </ListItem>
      </Link>
    </List>
  );
};
