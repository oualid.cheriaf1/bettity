from interface.RPCServer import RPCServer
from resources import session
from resources.user import User
class UserService(RPCServer):
    def get_user(self,user_id):
        """ Get the current user informations

            :param user_id: The user identifier
            :type user_id: int

            :return: User information without the password hash into a dict
            :rtype: dict
        """
        our_user = session.query(User).filter_by(id=user_id).first()
        return our_user.to_dict()
    def get_username_by_id(self, user_id):
        """ Get the user info as username by user id

            :param user_id: The user identifier
            :type user_id: int

            :return: Username of the user
            :rtype: string
        """
        user = session.query(User).filter_by(id=user_id).first()
        return user.username