import unittest 
from unittest.mock import patch
from src.helpers.RPCClient import RPCClient
import os
import dill
class RPCClientTest(unittest.TestCase):
    @patch("pika.BlockingConnection")
    @patch("pika.URLParameters")
    def test_rpc_client_init(self, URLParametersMock, BlockingConnectionMock):
        """ Should init a RPC client, declare an exclusive queue and consume message on this queue"""
        fake_service = RPCClient("fake_service_test", "fake_service_test-rk",
                  pika_connection_parameters=[URLParametersMock(os.getenv("RABBITMQ_URI"))])
        BlockingConnectionMock.assert_called_once_with(
            [URLParametersMock(os.getenv("RABBITMQ_URI"))])
        fake_service.connection.channel.assert_called_once()
        fake_service.channel.queue_declare.assert_called_once_with(queue="", exclusive=True)
        fake_service.channel.basic_consume.assert_called_once_with(queue=fake_service.callback_queue,
                                                                   on_message_callback=fake_service.on_response,
                                                                   auto_ack=True)

    @patch("pika.BasicProperties")
    @patch("pika.BlockingConnection")
    @patch("pika.URLParameters")
    def test_rpc_client_call_method(self, URLParametersMock, BlockingConnectionMock, BasicPropertiesMock):
        """ Should init a fake service and make an rpc call """
        fake_service = RPCClient("fake_service_test", "fake_service_test-rk",
                                 pika_connection_parameters=[URLParametersMock(os.getenv("RABBITMQ_URI"))])
        def side_effect():
            """Helper function to mock response"""
            fake_service.response = {"Hello Zorld": "Doyble Wa"}
        fake_service.connection.process_data_events.side_effect = side_effect
        fake_service.test1()
        fake_service.channel.basic_publish.assert_called_once_with(
            exchange="fake_service_test",
            routing_key="fake_service_test-rk",
            properties=BasicPropertiesMock(
                reply_to=fake_service.callback_queue,
                correlation_id=fake_service.corr_id,
                headers={"method": "test1"}
            ),
            body=dill.dumps({"args": (), "kwargs": {}}))
