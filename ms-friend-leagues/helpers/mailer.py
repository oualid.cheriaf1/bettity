import smtplib
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class Mailer:
    """ 
        Class to send email.

        :param auth_user: The user identifier. If gmail, it is the mail address. Required.
        :type auth_user: str. Required.

        :param auth_password: The user password. 
        :type auth_password: str. Required.

        :param host: The smtp server address.
        :type host: str. Optional. Default to `smtp-relay.gmail.com`.

        :param port: The port of the smtp server to send requests. 
        :type port: int. Optional. Default to 587.

        :ivar server: The server connection variable.
        :vartype server: class:`smtplib.SMTP`
    """

    def __init__(self, auth_user, auth_password, host="smtp-relay.gmail.com", port=587):
        self.auth_user = auth_user
        self.auth_password = auth_password
        self.host = host
        self.port = port
        self.server = smtplib.SMTP(host, port)
        self.server.starttls()
        self.server.login(auth_user, auth_password)

    def send_mail(self, sender, to, subject, html_content):
        """
            Send an email. 

            :param sender: The address mail of the sender.
            :type sender: str, required.

            :param to: The address mail to send the mail to.
            :type to: str, required.

            :param subject: The subject of the mail.
            :type subject: str, required.

            :param html_content: The body of the email. Should be html.
            :type html_content: str, required
        """
        msg = MIMEMultipart()
        msg["From"] = sender
        msg["To"] = to
        msg["Subject"] = subject
        msg.attach(MIMEText(html_content, "text"))
        msg.attach(MIMEText(html_content, "html"))
        try:
            self.server.send_message(msg)
        except smtplib.SMTPServerDisconnected:
            self.server.connect(self.host, self.port)
            self.server.send_message(msg)

