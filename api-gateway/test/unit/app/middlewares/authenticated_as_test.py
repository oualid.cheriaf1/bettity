import unittest
from unittest.mock import patch
from src.app.middlewares.authenticated_as import authenticated_as
from src.app.models.user import UserRole
class AuthenticatedAsTest(unittest.TestCase):
    def _build_protected_route(self, role):
        @authenticated_as(role)
        def protected_route():
            return True
        return protected_route
    def test_authenticated_as_is_middleware(self):
        """ Assert that authenticated_as is a decorator. """
        self.assertTrue(callable(authenticated_as))
        self.assertTrue(callable(authenticated_as(UserRole.admin)))
        @authenticated_as(UserRole.admin)
        def fake_route_controller():
            pass
        self.assertTrue(callable(fake_route_controller))
        self.assertEqual(fake_route_controller.__name__, "fake_route_controller")
