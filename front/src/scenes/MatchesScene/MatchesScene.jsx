import React from "react";

import { MatchList } from "../../components/MatchList/MatchList";
import { PageContent } from "../../components/PageContent/PageContent";
import { PageTitle } from "../../components/PageTitle/PageTitle";
import { useMatches } from "../../core/hooks/api/matches";

export const MatchesScene = () => {
  const matches = useMatches();
  return (
    <>
      <PageTitle title="Les résultats" />
      <PageContent>
        {matches && <MatchList matches={matches}></MatchList>}
      </PageContent>
    </>
  );
};
