import { useState, useEffect } from "react";

import { useApi } from "../useApi";

export const useStanding = leagueId => {
  const API = useApi(); // We retrieve the api function.
  const [standing, setStanding] = useState(); // Standing state
  const getUsername = async (userId, numberOfPoint) => {
    // Call to api to get the username of a user.
    // Useful to add username to standings objects
    const response = await API({
      url: `/user/${userId}`,
      method: "GET"
    });
    return {
      userId,
      numberOfPoint: numberOfPoint[userId],
      username: response.data.user
    };
  };
  const getLeagueStanding = async () => {
    // async function to retrieve standing

    // make http call to get standing of the league -> {[userId]:number of point for this user.}
    const json = await API({ url: `/leagues/${leagueId}`, method: "GET" });

    const numberOfPoint = json.data;
    const promises = [];
    for (let userId in numberOfPoint) {
      promises.push(getUsername(userId, numberOfPoint)); // Add promises to an array
    }

    const standings = await Promise.all(promises); // wait for all promises to be fullfilled

    standings.sort((a, b) => b.numberOfPoint - a.numberOfPoint); // Sort standing array on user's number of point
    setStanding(standings);
  };
  useEffect(() => {
    if (leagueId) {
      getLeagueStanding();
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [leagueId]);
  return standing;
};
