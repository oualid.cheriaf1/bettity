import os
import pymongo
import math
import pika
from interface.interface import RPCServer
from helpers.random_code import gen_random_code
from helpers import pymailer


class FriendLeagueService(RPCServer):
    """
        The class:`FriendLeagueService` manage the league between friends.
        It uses MongoDB and get the credentials from environment variable.

        :param exchange_name: The RabbitMQ exchange name.
        :type exchange_name: str, required.

        :param queue_name: The RabbitMQ queue name.
        :type queue_name: str, required.
    """

    def __init__(self, exchange_name, queue_name, pika_connection_parameters=[pika.URLParameters(os.getenv("RABBITMQ_URI"))]):
        """ Initialize the friend-league-service and connect to MongoDB """
        super().__init__(exchange_name=exchange_name, queue_name=queue_name,
                         pika_connection_parameters=pika_connection_parameters)
        MONGO_DB_URI = os.getenv("MONGO_DB_URI")
        self.db = pymongo.MongoClient(
            MONGO_DB_URI)["heroku_j4wtgbxw"]

    def create_league(self, owner_id, league_name):
        """ Create and save a league. Should generate a 6 alphanumeric code for the league. The owner_id is the user_id of the creator of the league.

            :param owner_id: User id of the league creator
            :type owner_id: int, required.

            :param league_name: Name of the league
            :type league_name: str, required.

            :return: League object
            :rtype: dict

            :raises Exception: Raises exception if an error occured.
        """
        leagues_collection = self.db.leagues_collection
        if leagues_collection.find_one({"league_name": league_name}):
            raise Exception('LEAGUE_NAME_ALREADY_TAKEN')
        # We generate a random code for each league. We have 36 ** 6 = 2 176 782 336 possibilities of code
        code = gen_random_code()
        while leagues_collection.find_one({"league_code": code}):
            code = gen_random_code()
        league = {"league_name": league_name,
                  "league_owner": owner_id, "league_code": code}
        leagues_collection.insert_one({**league})
        user_league_collection = self.db.user_league_collection  # Association collection
        user_league_collection.insert_one({**league, "user_id": owner_id})
        return league

    def add_user_to_league(self, user_id, league_code):
        """ Should register a user in a league.

            :param user_id: The user identifier of the user to register
            :type user_id: int, required.

            :param league_code: The 6 alphanumeric code of the league
            :type league_code: str, required.

            :return: a dict containing the league code, the league name, and the user id of the newly registered user
            :rtype: dict

            :raises Exception: Raises exception if an error occured.
        """
        leagues_collection = self.db.leagues_collection
        user_league_collection = self.db.user_league_collection  # Association Table
        league = leagues_collection.find_one({"league_code": league_code})
        if not league:
            raise Exception('LEAGUE_DOES_NOT_EXIST')
        if user_league_collection.find_one({"league_code": league['league_code'], "league_name": league["league_name"], "user_id": user_id}):
            raise Exception('USER_ALREADY_REGISTERED_IN_LEAGUE')
        user_league_collection.insert_one({**league, "user_id": user_id})
        return {"league_code": league['league_code'], "league_name": league["league_name"], "user_id": user_id}

    def get_user_leagues(self, user_id):
        """ Get all the league where the user is registred

            :param user_id: The user identifer
            :type user_id: int, required.

            :return: The list of league where the user is registered
            :rtype: list of dict.
        """
        leagues_collection = self.db.leagues_collection
        user_league_collection = self.db.user_league_collection  # Association Table
        league_with_mongo_id_list = []
        for user_registration in user_league_collection.find({"user_id": user_id}):
            league = leagues_collection.find_one(
                {"league_name": user_registration["league_name"]})
            league_with_mongo_id_list.append(league)
        league_list = [{"league_name": u["league_name"],
                        "league_code":u["league_code"],
                        "league_owner":u["league_owner"]} for u in league_with_mongo_id_list]
        return league_list

    def get_league_rankings(self, league_code):
        """ Get the league rankings.

            :param league_code: The 6 alphanumeric code of the league
            :type league_code: str, required.

            :return: A dict with user id as key and number of point as value for each user in league
            :rtype: dict

            :raises Exception: Raises exception if an error occured.
        """
        leagues_collection = self.db.leagues_collection
        # Check if league exist
        league = leagues_collection.find_one({"league_code": league_code})
        if not league:
            raise Exception('LEAGUE_DOES_NOT_EXIST')
        # Init all necessary collection
        user_league_collection = self.db.user_league_collection
        bets_collection = self.db.bets_collection
        match_collection = self.db.matches
        fifa_ranking_collection = self.db.fifa_ranking
        # Get all user registered in league
        user_registered_in_league = [u["user_id"] for u in user_league_collection.find(
            {"league_code": league["league_code"]})]
        bets_per_user = {}
        for user_id in user_registered_in_league:
            # Compute points for user with id = user_id
            number_of_points = self._compute_number_of_point_per_user(
                user_id, bets_collection, match_collection, fifa_ranking_collection)
            bets_per_user[user_id] = number_of_points
        return bets_per_user

    def invite_by_email(self, league_code, email_list, mailer=pymailer):
        """ Send an email to a list of user with the invite code.

            :param league_code: The league code.
            :type league_code: str, required.
            :param email_list: the email addresses
            :type email_list: list of str, required.
            :return: A dict indicating the success or the failure
            :rtype: dict
        """
        leagues_collection = self.db.leagues_collection
        league = leagues_collection.find_one({"league_code": league_code})
        if not league:
            raise Exception('LEAGUE_DOES_NOT_EXIST')
        subject = "Ton invitation pour rejoindre la ligue %s" % league_code  # The email subject
        # We build the email content.
        html_content = "<div class=\"container\">"
        html_content += "<h3> Ton invitation pour rejoindre la ligue %s </h3>" % league_code
        html_content += "<p> Pour rejoindre la ligue, connectez-vous sur <a href=\"bettity-dev.netlify.com/register\">Bettity</a> si ce n'est pas déjà fait.<br/>"
        html_content += " Puis utiliser le code <b>%s</b> pour rejoindre la ligue sur l'onglet <i>Mes ligues</i>.</p>" % league_code
        html_content += "</div>"
        # We send the email to each email address in email_list
        for email in email_list:
            mailer.send_mail("no-reply@coddity.com",
                             email, subject, html_content)
        return {"succcess": True}

    def _compute_number_of_point_per_user(self, user_id, bets_collection, match_collection, fifa_ranking_collection):
        number_of_points = 0
        for bet in bets_collection.find({"user_id": user_id}):
            match = match_collection.find_one({"id": bet['match_id']})
            point_for_this_bet = 0
            if match["status"] == "FINISHED":
                # We compute the earned point
                score = match["score"]
                predicted_result = "DRAW"
                if bet['scoreHome'] > bet['scoreAway']:
                    predicted_result = "HOME_TEAM"
                elif bet["scoreHome"] < bet['scoreAway']:
                    predicted_result = "AWAY_TEAM"
                if predicted_result == score['winner']:
                    # If the player has predicted the right match outcome,
                    # he earn 100 points.
                    point_for_this_bet += 100
                    home_team_fifa_point = fifa_ranking_collection.find_one(
                        {"name": match["homeTeam"]["name"]})
                    away_team_fifa_point = fifa_ranking_collection.find_one(
                        {"name": match["awayTeam"]["name"]})
                    # We use the FIFA point difference to give points.
                    # Let two teams A & B with points x and y respectively and such that x>y
                    # If the player has predicted that team A win and team A won, the player earn 1/2 |x-y| additionnal points
                    # If the player has predicted that team B win and team B won, the player earn 2 |x-y| additionnal points
                    # If the player has predicted that the match is a draw and the match is drawn, the player earn |x-y| additionnal points
                    point_difference = abs(
                        home_team_fifa_point["point"]-away_team_fifa_point['point'])
                    if predicted_result == "HOME_TEAM":
                        if home_team_fifa_point["point"] > away_team_fifa_point['point']:
                            point_for_this_bet += math.floor(
                                point_difference/2)
                        elif away_team_fifa_point['point'] > home_team_fifa_point['point']:
                            point_for_this_bet += 2 * \
                                math.floor(point_difference)
                    elif predicted_result == "AWAY_TEAM":
                        if home_team_fifa_point["point"] > away_team_fifa_point['point']:
                            point_for_this_bet += 2*math.floor(
                                point_difference)
                        elif away_team_fifa_point['point'] > home_team_fifa_point['point']:
                            point_for_this_bet += math.floor(
                                point_difference/2)
                    else:
                        point_for_this_bet += point_difference
                    # If the player has predicted the right score, he gets his match point doubled
                    if bet["scoreHome"] == score["fullTime"]["homeTeam"] and bet["scoreAway"] == score["fullTime"]["awayTeam"]:
                        point_for_this_bet *= 2
            number_of_points += point_for_this_bet
        return number_of_points
