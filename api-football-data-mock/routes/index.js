const router = require("express").Router();
const competitionRouter = require("./competitions");
router.use("/competitions", competitionRouter);

module.exports = router;
