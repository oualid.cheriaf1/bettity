import React, { useEffect, useState } from "react";

import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

import { LogoutButton } from "../../../components/LogoutButton/LogoutButton";
import { PageContent } from "../../../components/PageContent/PageContent";
import { PageTitle } from "../../../components/PageTitle/PageTitle";
import { useProfile } from "../../../core/hooks/api/users";
import { UserRole } from "../../../core/resources/user";

export const ProfileScene = () => {
  const profileRequest = useProfile();
  const [profileResponse, setProfileResponse] = useState();
  const getProfile = async () => {
    const { data } = await profileRequest();
    setProfileResponse(data);
  };
  useEffect(() => {
    getProfile();
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <PageTitle title="Profil" />
      <PageContent>
        {profileResponse && (
          <>
            <Box p={1} textAlign="left">
              <Typography variant="h6" color="secondary" component="span">
                {profileResponse.user.role === UserRole.admin
                  ? "Administrateur"
                  : "Utilisateur"}
              </Typography>
            </Box>

            <Paper elevation={0}>
              <Box p={2} textAlign="left">
                <Typography>
                  <Typography
                    variant="body1"
                    color="textSecondary"
                    component="span"
                  >
                    Email :
                  </Typography>
                  <Typography variant="body1" component="span">
                    {` ${profileResponse.user.email}`}
                  </Typography>
                </Typography>
                <Typography>
                  <Typography
                    variant="body1"
                    color="textSecondary"
                    component="span"
                  >
                    Nom d'utilisateur :
                  </Typography>
                  <Typography variant="body1" component="span">
                    {/* Stringify below to add space before */}
                    {` ${profileResponse.user.username}`}
                  </Typography>
                </Typography>
              </Box>
            </Paper>
            <LogoutButton />
          </>
        )}
      </PageContent>
    </>
  );
};
