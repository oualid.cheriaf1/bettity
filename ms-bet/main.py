import click
import sentry_sdk
from dotenv import load_dotenv
load_dotenv()
import os
sentry_sdk.init(os.getenv('SENTRY_DSN'))
@click.group()
def cli():
    pass

@cli.command()
def run():
    from app.app import BetService
    bet_service = BetService()
    bet_service.listen()

@cli.command()
def test():
    import unittest
    tests = unittest.TestLoader().discover('.', pattern="*_test.py")
    results = unittest.TextTestRunner(verbosity=2).run(tests)
    if results.wasSuccessful():
        return 0
    return 1

if __name__ == "__main__":
    cli()
