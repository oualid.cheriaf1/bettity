import uuid
import json
import pika
import dill
import os
class RPCClient:
    def __init__(self, exchange_name="", queue_name="", pika_connection_parameters=[pika.URLParameters(os.getenv("RABBITMQ_URI"))]):
        self.queue_name = queue_name
        self.exchange_name = exchange_name
        self.pika_connection_parameters = pika_connection_parameters
        self.connection = pika.BlockingConnection(
            self.pika_connection_parameters)
        self.channel = self.connection.channel()
        result = self.channel.queue_declare(queue='', exclusive=True)
        self.callback_queue = result.method.queue
        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            if props.headers and "error" in props.headers:
                print(dill.loads(body))
                raise dill.loads(body)
            self.response = dill.loads(body)
            return self.response
    
    def __getattr__(self, attr):
        def call(*args,**kwargs):
            self.response = None
            self.corr_id = str(uuid.uuid4())
            self.channel.basic_publish(
                exchange=self.exchange_name,
                routing_key=self.queue_name,
                properties=pika.BasicProperties(
                    reply_to=self.callback_queue,
                    correlation_id=self.corr_id,
                    headers={"method": attr}
                ),
                body=dill.dumps({"args":args,"kwargs":kwargs}))
            while self.response is None:
                self.connection.process_data_events()
            return self.response
        return call


if __name__ == "__main__":
    score = RPCClient("oualid", "oualid-rpc")
    try:
        print(score.test1(6))
    except Exception as e:
        from sentry_sdk import capture_exception
        capture_exception(e)
        print(e.args[0])

