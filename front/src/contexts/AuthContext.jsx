import React, { createContext, useReducer } from "react";
// Initial auth State
const initialState = {
  isConnected: localStorage.getItem("isConnected") === "true", // Retrieve from localStorage a string. Check if connected.
  token: localStorage.getItem("token") || undefined // Retrieve the token
};


// Reducer to modify the authState
const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      localStorage.setItem("isConnected", "true"); // on Connection, set isConnected to "true"
      localStorage.setItem("token", action.payload.token); // onConnection, use the token
      return { isConnected: true, ...action.payload };
    case "LOGOUT":
      // onLogout, delete the isConnected and token key from localStorage
      localStorage.removeItem("isConnected");
      localStorage.removeItem("token");
      return { isConnected: false };
    default:
      return state;
  }
};
export const AuthContext = createContext({
  // AuthState
  authState: initialState,
  // Actions
  loginAction: () => {},
  logoutAction: () => {}
});

export const AuthContextProvider = ({ children }) => {
  const [authState, dispatch] = useReducer(reducer, initialState);
  const loginAction = payload => {
    dispatch({ type: "LOGIN", payload });
  };
  const logoutAction = () => {
    dispatch({ type: "LOGOUT" });
  };
  return (
    <AuthContext.Provider value={{ authState, loginAction, logoutAction }}>
      {children}
    </AuthContext.Provider>
  );
};
