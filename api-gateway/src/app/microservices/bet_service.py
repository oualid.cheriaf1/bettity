from src.helpers.RPCClient import RPCClient


class BetService(RPCClient):
    def __init__(self):
        super().__init__("bet_service", "bet_service-rk")
