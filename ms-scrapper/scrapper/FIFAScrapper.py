import requests
from bs4 import BeautifulSoup
import os
from pymongo import MongoClient


class FIFATeamPointScrapper:
    """ 
        The application uses the FIFA standing point to compute the point for each bets. 
        This class let us build an object to scrap the FIFA standing and save the number of point into our Mongo database.

        :param url: The url of the FIFA standing page.
        :type url: str, required.

        :ivar html_content: The html content of the FIFA standing page.
        :vartype html_content: str
        
        :ivar standing: The standings list. 
        :vartype standing: list of dict.
    """
    
    def __init__(self, url):
        self.html_content = requests.get(url).text
        self.standing = []

    def extract(self):
        """ Helper method to extract info from Fifa standing page, ie from `html_content` """
        self.beautiful_soup = BeautifulSoup(
            self.html_content, features="html.parser")  # We parse the html_content using BeautifulSoup
        
        table = self.beautiful_soup.find('table', {"id": "rank-table"}) # We find the standing table on the page
        table_body = table.find('tbody')
        table_rows = table_body.find_all("tr") # Each row correspond to a team
        for row in table_rows:
            self._extract_info_from_row(row) 
        return self.standing

    def _extract_info_from_row(self, row):
        """ Helper method to extract info from a row which correspond to a team """
        team_name_cell = row.find(
            "td", {"class": 'fi-table__teamname'}) # We extract the team name
        team_name = team_name_cell.find(
            "span", {"class": "fi-t__nText"}).text.strip()
        team_point_cell = row.find("td", {"class": "fi-table__points"}) # We extract the number of point for the team
        team_point = int(team_point_cell.find("span").text.strip()) 
        self.standing.append({"name": team_name, "point": team_point}) # We save the informations into instance variable standing 

    def save_to_mongo(self):
        """ Helper method which initialize the mongo database connection and save into the db the standing."""
        mongo_uri = os.getenv("MONGO_DB_URI")
        client = MongoClient(mongo_uri)
        db = client["heroku_j4wtgbxw"]
        fifa_ranking = db.fifa_ranking
        for standing in self.standing:
            fifa_ranking.find_one_and_update({"name": standing["name"]}, {
                                             "$set": {**standing}}, upsert=True)

    def extract_and_save(self):
        self.extract()
        self.save_to_mongo()
