import unittest 
from helpers.mailer import Mailer
from unittest.mock import patch 
class MailerTest(unittest.TestCase): 
    """ Unit test of Mailer test """
    def setUp(self):
        self.fake_user = "fake_user@fake_domain.com"
        self.fake_password = "fake_password"
        self.fake_smtp_host = "fake-smtp.domain.com"
        self.fake_port = 90283

    
    @patch("smtplib.SMTP")
    def test_init_mailer_object(self, SMTPMock):
        """ Init a mailer object. """
        mailer_instance = Mailer(self.fake_user, self.fake_password)
        SMTPMock.assert_called_once_with("smtp-relay.gmail.com", 587)
        mailer_instance.server.starttls.assert_called_once()
        mailer_instance.server.login.assert_called_once_with(
            self.fake_user, self.fake_password)
        SMTPMock.reset_mock()
        another_mailer_instance = Mailer(
            self.fake_user, self.fake_password, host=self.fake_smtp_host, port=self.fake_port)
        SMTPMock.assert_called_once_with(self.fake_smtp_host, self.fake_port)
        another_mailer_instance.server.starttls.assert_called_once()
        another_mailer_instance.server.login.assert_called_once_with(
            self.fake_user, self.fake_password)

    @patch("email.mime.text.MIMEText")
    @patch("email.mime.multipart.MIMEMultipart")
    @patch("smtplib.SMTP")
    def test_send_mail(self, SMTPMock, MIMEMultipartMock, MIMETextMock):
        """ Send an email using the Mailer class """
        mailer_instance = Mailer(self.fake_user, self.fake_password)
        # We send an email. 
        # Should build a MIMEMultipart object with right informations 
        # and use the SMTP connection to send the message.
        sender = "fake_sender@fake-domain.com"
        receiver = "fake_mail_address@another-fake-domain.com"
        subject = "A test of mailer class"
        html_content = "<div><p> A fake email to test the mailer class</p></div>"

        mailer_instance.send_mail(sender, receiver, subject, html_content)
        mailer_instance.server.send_message.assert_called_once()