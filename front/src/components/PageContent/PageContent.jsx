import React from "react";

import Box from "@material-ui/core/Box";

export const PageContent = ({ children }) => {
  return (
    <Box textAlign="center" py={2}>
      {children}
    </Box>
  );
};
