import unittest
from unittest.mock import patch
from app.app import BetService
from interface.interface import RPCServer


class BetServiceTest(unittest.TestCase):

    def _find_one_and_update_mock(self, criteria, update, upsert=False):
        has_update_element = False
        new_collection = []
        for element in self.bets_collection_mock:
            match_criterium = True
            for key in criteria:
                if key not in element or criteria[key] != element[key]:
                    match_criterium = False
                    break
            if match_criterium == True and not has_update_element:
                has_update_element = True
                new_collection.append(update["$set"])
            if not match_criterium:
                new_collection.append(element)
        if not has_update_element and upsert:
            new_collection.append(update["$set"])
        self.bets_collection_mock = new_collection

    def _mock_find(self, criteria):
        response_list = []
        for element in self.bets_collection_mock:
            match_criterium = True
            for key in criteria:
                if key not in element or criteria[key] != element[key]:
                    match_criterium = False
                    break
            if match_criterium:
                response_list.append(element)
        return response_list

    def setUp(self):
        self.bets = [
            {"matchId": 3892, "match_id": 3892, "scoreHome": 0,
                "scoreAway": 4, "matchUtcDate": "2020-04-12T14:33:57.384Z"},
            {"matchId": 3894, "match_id": 3894, "scoreHome": 3,
                "scoreAway": 1, "matchUtcDate": "2020-04-12T14:33:57.384Z"}
        ]
        self.bets_after_match_date = [
            {"matchId": 3894, "match_id": 3894, "scoreHome": 0,
                "scoreAway": 4, "matchUtcDate": "2020-01-01T14:33:57.384Z"},
            {"matchId": 3895, "match_id": 3895, "scoreHome": 2,
                "scoreAway": 1, "matchUtcDate": "2020-01-01T14:33:57.384Z"}
        ]
        self.bets_collection_mock = []

    @patch("pika.BlockingConnection")
    @patch('pika.URLParameters')
    @patch("pymongo.MongoClient")
    def test_init_bet_service(self, MongoClientMock, URLParametersMock, BlockingConnectionMock):
        """ Assert that BetService inherit from RPCServer and initialize a BetService instance"""
        self.assertTrue(issubclass(BetService, RPCServer))
        bet_service = BetService(pika_connection_parameters=[
                                 URLParametersMock()])
        self.assertTrue(hasattr(bet_service, "save_bets"))
        self.assertTrue(hasattr(bet_service, "get_all_bets_by_user_id"))
        MongoClientMock.assert_called_once()

    @patch("pika.BlockingConnection")
    @patch('pika.URLParameters')
    @patch("pymongo.MongoClient")
    def test_save_bets(self, MongoClientMock, URLParametersMock, BlockingConnectionMock):
        bet_service = BetService(pika_connection_parameters=[
                                 URLParametersMock()])
        bet_service.db.bets_collection.find_one_and_update.side_effect = self._find_one_and_update_mock
        bet_service.save_bets(1, self.bets)
        bet_service.save_bets(2, self.bets)
        self.assertEqual(4, len(self.bets_collection_mock))

    @patch("pika.BlockingConnection")
    @patch('pika.URLParameters')
    @patch("pymongo.MongoClient")
    def test_get_bets(self, MongoClientMock, URLParametersMock, BlockingConnectionMock):
        bet_service = BetService(pika_connection_parameters=[
                                 URLParametersMock()])

        bet_service.db.bets_collection.find.side_effect = self._mock_find
        result = bet_service.get_all_bets_by_user_id(0)
        self.assertListEqual(result, [])

    @patch("pika.BlockingConnection")
    @patch('pika.URLParameters')
    @patch("pymongo.MongoClient")
    def test_save_then_get_bets(self, MongoClientMock, URLParametersMock, BlockingConnectionMock):
        bet_service = BetService(pika_connection_parameters=[
                                 URLParametersMock()])
        bet_service.db.bets_collection.find_one_and_update.side_effect = self._find_one_and_update_mock
        bet_service.db.bets_collection.find.side_effect = self._mock_find
        bet_service.save_bets(1, self.bets)
        result = bet_service.get_all_bets_by_user_id(0)
        self.assertListEqual(result, [])
        result = bet_service.get_all_bets_by_user_id(1)
        self.assertListEqual(
            result, [{"matchId": 3892, "scoreHome": 0,
                      "scoreAway": 4},
                     {"matchId": 3894, "scoreHome": 3,
                      "scoreAway": 1}])

    @patch("pika.BlockingConnection")
    @patch('pika.URLParameters')
    @patch("pymongo.MongoClient")
    def test_save_bet_after_match_date(self, MongoClientMock, URLParametersMock, BlockingConnectionMock):
        """ The user should not be able to save his bet if the match date is past"""
        bet_service = BetService(pika_connection_parameters=[
                                 URLParametersMock()])
        bet_service.db.bets_collection.find_one_and_update.side_effect = self._find_one_and_update_mock
        bet_service.db.bets_collection.find.side_effect = self._mock_find
        with self.assertRaises(Exception):
            bet_service.save_bets(1, self.bets_after_match_date)
