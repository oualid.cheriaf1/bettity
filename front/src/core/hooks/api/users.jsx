import { useApi } from "../useApi";

export const useProfile = () => {
  const API = useApi();
  return async () => API({ url: "/user/me", method: "GET" });
};
