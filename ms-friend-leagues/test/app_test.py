import unittest
from unittest.mock import patch
from app.app import FriendLeagueService
from interface.interface import RPCServer
import random


class TestFriendLeagueService(unittest.TestCase):
    """ 
        The test suite of the FriendLeagueService. 
    """

    def _build_insert_one_mock(self, collection):
        def _insert_one_mock(document):
            collection.append(document)
        return _insert_one_mock

    def _build_find_one_and_update_mock(self, collection):
        def _find_one_and_update_mock(criteria, update, upsert=False):
            has_update_element = False
            new_collection = []
            for element in collection:
                match_criterium = True
                for key in criteria:
                    if key not in element or criteria[key] != element[key]:
                        match_criterium = False
                        break
                if match_criterium == True and not has_update_element:
                    has_update_element = True
                    new_collection.append(update["$set"])
                if not match_criterium:
                    new_collection.append(element)
            if not has_update_element and upsert:
                new_collection.append(update["$set"])
            collection = new_collection
        return _find_one_and_update_mock

    def _build_find_mock(self, collection):
        def _find_mock(criteria):
            response_list = []
            for element in collection:
                match_criterium = True
                for key in criteria:
                    if key not in element or criteria[key] != element[key]:
                        match_criterium = False
                        break
                if match_criterium:
                    response_list.append(element)
            return response_list
        return _find_mock

    def _build_find_one_mock(self, collection):
        def _find_one_mock(criteria):
            for element in collection:
                match_criterium = True
                for key in criteria:
                    if key not in element or criteria[key] != element[key]:
                        match_criterium = False
                        break
                if match_criterium:
                    return element
            return []
        return _find_one_mock

    def setUp(self):
        """ Before each test """
        self.leagues_collection = []
        self.user_league_collection = []

    def tearDown(self):
        """ After each test """
        pass

    @patch("pika.BlockingConnection")
    @patch("pika.URLParameters")
    @patch("pymongo.MongoClient")
    def test_init_service(self, MongoClientMock, URLParametersMock, BlockingConnectionMock):
        """ Should init the service and verify that all the required methods are present. """
        self.assertTrue(issubclass(FriendLeagueService, RPCServer))
        friends_league_service = FriendLeagueService("fake_friend_league_service",
                                                     "fake_friend_league_service-rk",
                                                     pika_connection_parameters=[URLParametersMock("amqp")])
        self.assertTrue(hasattr(friends_league_service, "create_league"))
        self.assertTrue(hasattr(friends_league_service, "add_user_to_league"))
        self.assertTrue(hasattr(friends_league_service, "get_user_leagues"))
        self.assertTrue(hasattr(friends_league_service,
                                "get_league_classements"))
        self.assertTrue(hasattr(friends_league_service, "invite_by_email"))
        MongoClientMock.assert_called_once()

    @patch("pika.BlockingConnection")
    @patch("pika.URLParameters")
    @patch("pymongo.MongoClient")
    def test_create_league(self, MongoClientMock, URLParametersMock, BlockingConnectionMock):
        """ Should generate a random key for the league, and set the current user as league owner.
            Should also save the league in the mongo database
            Should fail if league name already exists.
        """
        friends_league_service = FriendLeagueService("fake_friend_league_service", "fake_friend_league_service-rk", pika_connection_parameters=[
            URLParametersMock()])
        friends_league_service.db.leagues_collection.find_one.side_effect = self._build_find_one_mock(
            self.leagues_collection)
        friends_league_service.db.leagues_collection.insert_one.side_effect = self._build_insert_one_mock(
            self.leagues_collection)
        friends_league_service.db.user_league_collection.insert_one.side_effect = self._build_insert_one_mock(
            self.user_league_collection)
        league_names = ["La ligue de Fabien",
                        "La ligue de Dimitri",
                        "La ligue de Fabien",
                        "La ligue de Jojo",
                        "La ligue d'Olivier"]
        random.seed(a=42)
        friends_league_service.create_league(0, league_names[0])
        self.assertEqual(
            friends_league_service.db.leagues_collection.find_one.call_count, 2)
        self.assertEqual(
            friends_league_service.db.leagues_collection.insert_one.call_count, 1)
        self.assertEqual(
            friends_league_service.db.user_league_collection.insert_one.call_count, 1)
        self.assertIn({"league_name": "La ligue de Fabien",
                       "league_owner": 0, "league_code": "82IYT9"}, self.leagues_collection)
        self.assertIn({"league_name": "La ligue de Fabien",
                       "league_owner": 0, "league_code": "82IYT9", "user_id": 0}, self.user_league_collection)

        # Should not have the same league_code
        random.seed(a=42)
        friends_league_service.create_league(0, league_names[1])
        self.assertEqual(
            friends_league_service.db.leagues_collection.find_one.call_count, 2+3)
        self.assertEqual(
            friends_league_service.db.leagues_collection.insert_one.call_count, 1+1)
        self.assertEqual(
            friends_league_service.db.user_league_collection.insert_one.call_count, 1+1)
        self.assertIn({"league_name": "La ligue de Fabien",
                       "league_owner": 0, "league_code": "82IYT9"}, self.leagues_collection)
        self.assertIn({"league_name": "La ligue de Dimitri", "league_owner": 0,
                       "league_code": "7B6K32"}, self.leagues_collection)
        self.assertEqual(len(self.leagues_collection), 2)
        self.assertIn({"league_name": "La ligue de Fabien",
                       "league_owner": 0, "league_code": "82IYT9", "user_id": 0}, self.user_league_collection)
        self.assertIn({"league_name": "La ligue de Dimitri", "league_owner": 0,
                       "league_code": "7B6K32", "user_id": 0}, self.user_league_collection)
        # Should fail to create the league if the league name already exists.
        with self.assertRaises(Exception) as cm:
            friends_league_service.create_league(0, league_names[1])
        the_exception = cm.exception
        self.assertEqual(the_exception.args, ("LEAGUE_NAME_ALREADY_TAKEN",))

    @patch("pika.BlockingConnection")
    @patch("pika.URLParameters")
    @patch("pymongo.MongoClient")
    def test_add_user_to_league(self, MongoClientMock, URLParametersMock, BlockingConnectionMock):
        """ Should register a user into a league. """
        friends_league_service = FriendLeagueService("fake_friend_league_service", "fake_friend_league_service-rk", pika_connection_parameters=[
            URLParametersMock()])
        friends_league_service.db.leagues_collection.find_one.side_effect = self._build_find_one_mock(
            self.leagues_collection)
        friends_league_service.db.leagues_collection.insert_one.side_effect = self._build_insert_one_mock(
            self.leagues_collection)
        friends_league_service.db.user_league_collection.insert_one.side_effect = self._build_insert_one_mock(
            self.user_league_collection)
        friends_league_service.db.user_league_collection.find_one.side_effect = self._build_find_one_mock(
            self.user_league_collection)

        # Assert raises an error when trying to add user to inexistant league
        with self.assertRaises(Exception) as cm:
            friends_league_service.add_user_to_league(12, "LEKLSL")
        friends_league_service.db.leagues_collection.find_one.assert_called_with(
            {"league_code": "LEKLSL"})
        self.assertEqual(cm.exception.args, ("LEAGUE_DOES_NOT_EXIST",))

        # Assert add a user to a league
        # So we create a league
        random.seed(a=42)
        friends_league_service.create_league(1, "Test-League")
        self.assertIn({"league_code": "82IYT9",  'league_owner': 1,
                       "league_name": "Test-League",
                       "user_id": 1}, self.user_league_collection)
        friends_league_service.add_user_to_league(2, "82IYT9")
        self.assertEqual(len(self.user_league_collection), 2)
        self.assertIn({"league_code": "82IYT9", 'league_owner': 1,
                       "league_name": "Test-League", "user_id": 2}, self.user_league_collection)

        # Assert raises an error if you try to register two time to same league
        with self.assertRaises(Exception) as cm:
            friends_league_service.add_user_to_league(2, "82IYT9")
        self.assertEqual(cm.exception.args,
                         ("USER_ALREADY_REGISTERED_IN_LEAGUE",))

    @patch("pika.BlockingConnection")
    @patch("pika.URLParameters")
    @patch("pymongo.MongoClient")
    def test_get_user_leagues(self, MongoClientMock, URLParametersMock, BlockingConnectionMock):
        """ Should retrieve all the league where the user is registered """
        friends_league_service = FriendLeagueService("fake_friend_league_service", "fake_friend_league_service-rk", pika_connection_parameters=[
            URLParametersMock()])
        friends_league_service.db.leagues_collection.find_one.side_effect = self._build_find_one_mock(
            self.leagues_collection)
        friends_league_service.db.leagues_collection.insert_one.side_effect = self._build_insert_one_mock(
            self.leagues_collection)
        friends_league_service.db.user_league_collection.insert_one.side_effect = self._build_insert_one_mock(
            self.user_league_collection)
        friends_league_service.db.user_league_collection.find_one.side_effect = self._build_find_one_mock(
            self.user_league_collection)
        friends_league_service.db.user_league_collection.find.side_effect = self._build_find_mock(
            self.user_league_collection)
        # Should return an empty list if the user is not registered in any league
        league_list = friends_league_service.get_user_leagues(0)
        self.assertEqual(league_list, [])
        self.assertEqual(
            friends_league_service.db.user_league_collection.find.call_count, 1)
        self.assertEqual(
            friends_league_service.db.leagues_collection.find_one.call_count, 0)

        # Should return all the league where the user is registered.
        random.seed(42)
        friends_league_service.create_league(0, "Test-League")
        league_list = friends_league_service.get_user_leagues(0)
        self.assertEqual(league_list, [
                         {"league_code": "82IYT9", "league_name": "Test-League", "league_owner": 0}])
        friends_league_service.create_league(1, "Test-League1")
        friends_league_service.add_user_to_league(0, "7B6K32")
        league_list = friends_league_service.get_user_leagues(0)
        self.assertEqual(league_list, [
                         {"league_code": "82IYT9",
                             "league_name": "Test-League", "league_owner": 0},
                         {'league_code': '7B6K32', 'league_name': 'Test-League1', 'league_owner': 1}])

    @patch("helpers.pymailer")
    @patch("pika.BlockingConnection")
    @patch("pika.URLParameters")
    @patch("pymongo.MongoClient")
    def test_invite_by_email(self, MongoClientMock, URLParametersMock, BlockingConnectionMock, pymailer_mock):
        """ Should try to send an email with the code and the procedure to invite someone to join the league """
        friends_league_service = FriendLeagueService("fake_friend_league_service", "fake_friend_league_service-rk", pika_connection_parameters=[
            URLParametersMock()])
        friends_league_service.db.leagues_collection.find_one.side_effect = self._build_find_one_mock(
            self.leagues_collection)
        friends_league_service.db.leagues_collection.insert_one.side_effect = self._build_insert_one_mock(
            self.leagues_collection)
        friends_league_service.db.user_league_collection.insert_one.side_effect = self._build_insert_one_mock(
            self.user_league_collection)
        friends_league_service.db.user_league_collection.find_one.side_effect = self._build_find_one_mock(
            self.user_league_collection)
        friends_league_service.db.user_league_collection.find.side_effect = self._build_find_mock(
            self.user_league_collection)

        # Assert Raise an error if you try to invite someone with a wrong code
        with self.assertRaises(Exception) as cm:
            friends_league_service.invite_by_email(
                "LDKELF", ["ocheriaf@gmail.com"])
        self.assertEqual(cm.exception.args, ("LEAGUE_DOES_NOT_EXIST",))
        pymailer_mock.send_mail.side_effect = [None, None]
        # We create a league to send the invite
        random.seed(a=42)
        friends_league_service.create_league(0, "League-Test")
        code_league = "82IYT9"
        friends_league_service.invite_by_email(
            code_league, ["ocheriaf@gmail.com"], pymailer_mock)
        pymailer_mock.send_mail.assert_called_once_with("no-reply@coddity.com", "ocheriaf@gmail.com", "Ton invitation pour rejoindre la ligue 82IYT9",
                                                        "<div class=\"container\"><h3> Ton invitation pour rejoindre la ligue 82IYT9 </h3><p> Pour rejoindre la ligue, connectez-vous sur <a href=\"bettity-dev.netlify.com/register\">Bettity</a> si ce n\'est pas déjà fait.<br/> Puis utiliser le code <b>82IYT9</b> pour rejoindre la ligue sur l\'onglet <i>Mes ligues</i>.</p></div>")
