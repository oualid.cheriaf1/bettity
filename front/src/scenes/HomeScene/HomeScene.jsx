import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { Paper } from "@material-ui/core";
export const HomeScene = () => {
  return (
    <>
      <Box textAlign="center">
        <Typography variant="h1">Bettity</Typography>
      </Box>
      <Box>
        <Typography variant="h3" color="primary">
          Le principe
        </Typography>
        <Paper elevation={0}>
          <Box p={2} textAlign="center" m={1}>
            <Typography>
              Inscrivez-vous, pariez sur les matchs de l'EURO 2020, créez ou
              rejoignez une ligue entre amis, et comparez votre classement avec
              eux.
            </Typography>
          </Box>
        </Paper>
        <Typography variant="h3" color="primary">
          Les règles
        </Typography>
        <Paper elevation={0}>
          <Box p={2} m={1}>
            Une fois vos paris faits, vous gagnez un certain nombre de points
            par match si vous avez prédit le bon vainqueur (ou match nul) et que
            le résultat du match y correspond. Vous doublez ce nombre de points
            si vous avez prédit le score exact du match.
          </Box>
          <Box p={2} m={1}>
            Le nombre de points gagnés par match dépend du classement FIFA des
            deux équipes. Concrétement, si l'équipe A joue contre l'équipe B,
            que l'équipe A a n<sub>A</sub> points et que l'équipe B a n
            <sub>B</sub> points, et que n<sub>A</sub> > n<sub>B</sub>, alors :
            <ul>
              <li>
                Si le joueur prédit la victoire de l'équipe A, et que l'équipe A
                gagne, le joueur gagne 100 + (n<sub>A</sub>-n<sub>B</sub>)/2
                points
              </li>
              <li>
                Si le joueur prédit la victoire de l'équipe B , et que l'équipe
                B gagne, le joueur gagne 100 + (n<sub>A</sub>-n<sub>B</sub>)*2
                points
              </li>
              <li>
                Si le joueur prédit le match nul et que le match se termine sur un
                match nul, alors le joueur gagne 100 + (n<sub>A</sub>-n
                <sub>B</sub>) points
              </li>
              <li>Si le joueur se trompe, il ne gagnera aucun point.</li>
            </ul>
            <Typography color="secondary">
              Si de plus le joueur prédit le score exact, il voit le nombre de
              points gagnés pour le match DOUBLE.
            </Typography>
          </Box>
        </Paper>
      </Box>
    </>
  );
};
