from src.helpers.RPCClient import RPCClient


class EuroLeagueService(RPCClient):
    def __init__(self):
        super().__init__("euro_league_service", "euro_league_service-rk")
