import enum
from sqlalchemy import Column, Integer, String, Enum, Sequence
from resources import Base


class UserRole(enum.Enum):
    """
        An enum of the possible user role
    """
    user = 1
    admin = 2


class User(Base):
    """ A class to represent a user """

    __tablename__ = "user"

    id = Column(
        Integer, Sequence("user_id_seq"), primary_key=True) 

    username = Column(String(64), index=True,
                      unique=True) 

    email = Column(String(120), index=True,
                   unique=True)

    password_hash = Column(
        String(128))

    role = Column(
        Enum(UserRole))

    def __init__(self, username, email, password_hash, role=UserRole.user):
        """ 
            Constructor which set the default fields and hash the password.

            :param username: The username of the user
            :type username: str, required.
            :param email: The email address of the user
            :type email: str, required
            :param password_hash: The password hash.
            :type password_hash: str, required.
            :param role: The role of the user.
            :type role: class:`UserRole`, optional, default to `UserRole.user`
        """
        self.username = username
        self.email = email
        self.password_hash = password_hash
        self.role = role

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def to_dict(self):
        return {"email": self.email, "id": self.id, "username": self.username, "role": self.role.value}
