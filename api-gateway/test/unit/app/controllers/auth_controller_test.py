import json
from test.unit import BaseTest
from unittest.mock import patch
from test.test_utils.test_utils import build_mocked_request_post

mocked_requests_post = build_mocked_request_post({"success":True}, 200)


class TestAuthController(BaseTest):
    """Tests of the AuthController."""


    @patch('requests.post', side_effect=mocked_requests_post)
    def test_register_user(self, post_function):
        """ Should register an unexistant user """
        with self.client:
            response = self.register_user(
                {"email": "fake@domain.tld", "password": "fake-password", "username": "fake-username"})
            self.assertEqual(response.status_code, 201)
            self.assertEqual(response.content_type, "application/json")
            data = json.loads(response.data.decode())
            self.assertEqual(data['status'], 201)
            self.assertEqual(data["message"], "L'utilisateur a bien été crée.")

    @patch('requests.post', side_effect=mocked_requests_post)
    def test_register_with_invalid_data(self, post_function):
        """ Should fail to register when the email or the username is already taken """
        with self.client:
            # Create the first user.
            response = self.register_user(
                {"email": "fake@domain.tld", "password": "fake-password", "username": "fake-username"})
            self.assertEqual(response.status_code, 201)
            self.assertEqual(response.content_type, "application/json")
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 201)
            self.assertEqual(data["message"], "L'utilisateur a bien été crée.")
            # Try to register with an existant email
            response = self.register_user(
                {"email": "fake@domain.tld", "password": "fake-password11", "username": "fake-username1"})
            self.assertTrue(response.status_code == 403)
            self.assertTrue(response.content_type == "application/json")
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 403)
            self.assertEqual(
                data["message"], "L'utilisateur existe déjà. Veuillez vous connecter")
            # Try to register with an existant username
            response = self.register_user(
                {"email": "fake1@domain.tld", "password": "fake-password11", "username": "fake-username"})
            self.assertTrue(response.status_code == 403)
            self.assertTrue(response.content_type == "application/json")
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 403)
            self.assertEqual(
                data["message"], "L'utilisateur existe déjà. Veuillez vous connecter")

    @patch('requests.post', side_effect=mocked_requests_post)
    def test_register_with_incomplete_data(self, post_function):
        """ Should fail to register if a required field is missing from the request body """
        with self.client:
            response = self.register_user(
                {"email": "fake@domain.tld", "password": "fake-password"})
            self.assertTrue(response.status_code == 422)
            self.assertTrue(response.content_type == "application/json")
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 422)
            self.assertEqual(
                data["message"], "Les données fournies sont invalides ou incomplètes")
            response = self.register_user(
                {"email": "fake@domain.tld", "username": "fail1"})
            self.assertTrue(response.status_code == 422)
            self.assertTrue(response.content_type == "application/json")
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 422)
            self.assertEqual(
                data["message"], "Les données fournies sont invalides ou incomplètes")
            response = self.register_user(
                {"password": "fake-password", "username": "fail1"})
            self.assertTrue(response.status_code == 422)
            self.assertTrue(response.content_type == "application/json")
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 422)
            self.assertEqual(
                data["message"], "Les données fournies sont invalides ou incomplètes")

    @patch('requests.post', side_effect=mocked_requests_post)
    def test_login_user(self, post_function):
        """ Should login a user """
        with self.client:
            # Create a new user first
            response = self.register_user(
                {"email": "fake@domain.tld", "password": "fake-password", "username": "fake-username"})
            # Login with the previous user
            response = self.login_user(
                {"email": "fake@domain.tld", "password": "fake-password"})
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.content_type == "application/json")
            data = json.loads(response.data.decode())
            self.assertTrue(data["status"] == 200)
            self.assertEqual(
                data["message"], "L'utilisateur s'est bien connecté")
            self.assertIn("data", data)
            self.assertIn("token", data["data"])

    @patch('requests.post', side_effect=mocked_requests_post)
    def test_login_with_invalid_credentials(self, post_function):
        """ Should fail to log with invalid credentials"""
        with self.client:
            # Create a new user first
            response = self.register_user(
                {"email": "fake@domain.tld", "password": "fake-password", "username": "fake-username"})
            # Try to log with a wrong password
            response = self.login_user(
                {"email": "fake@domain.tld", "password": "wrong-password"})
            self.assertEqual(response.status_code, 401)
            self.assertTrue(response.content_type == "application/json")
            data = json.loads(response.data.decode())
            self.assertTrue(data["status"], 401)
            self.assertEqual(data['message'], 'Le mot de passe est incorrecte')
            # Try to log with a wrong email
            response = self.login_user(
                {"email": "wrong@domain.tld", "password": "fake-password"})
            data = json.loads(response.data.decode())
            self.assertTrue(response.status_code == 404)
            self.assertTrue(response.content_type == "application/json")
            self.assertTrue(data["status"], 404)
            self.assertEqual(
                data['message'], "L'utilisateur n'a pas été trouvé")
            # Try to log without a password
            response = self.login_user(
                {"email": "fake@domain.tld"})
            data = json.loads(response.data.decode())
            self.assertTrue(response.status_code == 422)
            self.assertTrue(response.content_type == "application/json")
            self.assertTrue(data["status"], 422)
            self.assertEqual(
                data['message'], 'Les données fournies sont invalides ou incomplètes')
            # Try to log without an email
            response = self.login_user(
                {"password": "wrong-password"})
            data = json.loads(response.data.decode())
            self.assertTrue(response.status_code == 422)
            self.assertTrue(response.content_type == "application/json")
            self.assertTrue(data["status"], 422)
            self.assertEqual(
                data['message'], 'Les données fournies sont invalides ou incomplètes')
