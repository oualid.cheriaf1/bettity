import { useEffect, useState } from "react";
import * as moment from "moment";

import { useApi } from "../useApi";

export const useBets = matches => {
  const API = useApi();
  const [bets, setBets] = useState({});
  const getBets = async () => {
    const {
      data: { bets_list }
    } = await API({ url: "/bets/", method: "GET" });
    return bets_list;
  };
  useEffect(() => {
    const addMatchArrayToPossibleBets = matchArray => {
      const newBetSceneState = {};
      matchArray.forEach(match => {
        if (moment.utc(match.utcDate).isAfter(moment.utc())) {
          newBetSceneState[match.id] = {
            matchId: match.id,
            scoreHome: 0,
            scoreAway: 0,
            matchUtcDate: match.utcDate
          };
        }
      });
      return newBetSceneState;
    };
    let newState = { ...bets };
    if (matches) {
      Object.keys(matches).forEach(stage => {
        if (stage === "GROUP_STAGE") {
          Object.keys(matches[stage]).forEach(groupName => {
            newState = {
              ...newState,
              ...addMatchArrayToPossibleBets(matches[stage][groupName])
            };
          });
        } else {
          newState = {
            ...newState,
            ...addMatchArrayToPossibleBets(matches[stage])
          };
        }
      });
    }
    getBets().then(bets_list => {
      for (let i = 0; i < bets_list.length; i++) {
        newState = {
          ...newState,
          [bets_list[i].matchId]: {
            ...newState[bets_list[i].matchId],
            ...bets_list[i]
          }
        };
      }
      setBets(newState);
    });
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [matches]);
  return { bets, setBets };
};
