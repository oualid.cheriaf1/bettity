import os
import sentry_sdk
from dotenv import load_dotenv
load_dotenv(verbose=True)
sentry_sdk.init(os.getenv('SENTRY_DSN'))

from scrapper.FIFAScrapper import FIFATeamPointScrapper

url = os.getenv("FIFA_MEN_WORLD_RANKING_URL")

FIFATeamPointScrapper(url).extract_and_save()

