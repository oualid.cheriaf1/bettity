from flask import Blueprint
from src.helpers.error_handler import ErrorHandler
from src.helpers.response_normalizer import response_normalizer
from src.app.microservices.euro_league_service import EuroLeagueService
import pika
from src.app import cache


euro_league_controller = Blueprint(
    'euro_league_controller', __name__, url_prefix="/euro")


@euro_league_controller.route('/', methods=["GET"])
@cache.cached(timeout=50)
@ErrorHandler
def get_leagues():
    """ Route to get the competitions details. Cached."""
    competitions = EuroLeagueService().get_competition_details()
    return response_normalizer("COMPETITIONS_DETAILS_RETRIEVED",  competitions)


@euro_league_controller.route('/teams', methods=["GET"])
@cache.cached(timeout=60)
@ErrorHandler
def get_competition_teams():
    """ Route to get the teams registered in the competitions. Cached."""
    retrieved_teams = EuroLeagueService().get_competition_teams()
    return response_normalizer("COMPETITIONS_TEAMS_RETRIEVED",  retrieved_teams)



@euro_league_controller.route('/matches', methods=["GET"])
@cache.cached(timeout=60)
@ErrorHandler
def get_competition_matches():
    """ Route to get the competitions matches. Cached """
    matches = EuroLeagueService().get_competition_matches()
    return response_normalizer("COMPETITIONS_MATCHES_RETRIEVED",  matches)
