import React from "react";
import { Route, Switch } from "react-router-dom";

import Container from "@material-ui/core/Container";
import { makeStyles, createStyles } from "@material-ui/core/styles";

import { Navbar } from "../components/Navbar/Navbar";
import { PrivateRoute } from "../components/PrivateRoute/PrivateRoute";
import { HomeScene } from "./HomeScene/HomeScene";
import { MatchesScene } from "./MatchesScene/MatchesScene";
import { RegisterScene } from "./RegisterScene/RegisterScene";
import { TeamsScene } from "./TeamsScene/TeamsScene";
import { BetScene } from "./User/BetScene/BetScene";
import { LeagueScene } from "./User/LeagueScene/LeagueScene";
import { ProfileScene } from "./User/ProfileScene/ProfileScene";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      backgroundColor: theme.palette.background.default,
      color: theme.palette.text.primary,
      minHeight: "100vh"
    }
  })
);
export const AppContent = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Navbar />
      <Container>
        <Switch>
          <PrivateRoute path="/user/profile">
            <ProfileScene />
          </PrivateRoute>
          <PrivateRoute path="/user/bets">
            <BetScene />
          </PrivateRoute>
          <PrivateRoute path="/user/leagues">
            <LeagueScene />
          </PrivateRoute>
          <Route path="/register">
            <RegisterScene />
          </Route>
          <Route path="/teams">
            <TeamsScene />
          </Route>
          <Route path="/matches">
            <MatchesScene />
          </Route>
          <Route path="/">
            <HomeScene />
          </Route>
        </Switch>
      </Container>
    </div>
  );
};
