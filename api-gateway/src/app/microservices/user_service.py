from src.helpers.RPCClient import RPCClient

class UserService(RPCClient):
    def __init__(self):
        super().__init__("user_service", "user_service-rk")
