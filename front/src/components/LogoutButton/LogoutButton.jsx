import React, { useContext } from "react";

import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";

import { AuthContext } from "../../contexts/AuthContext";

/**
 * A logout button.
 */
export const LogoutButton = () => {
  const { logoutAction } = useContext(AuthContext); // Use the logout function provided by the AuthContext
  return (
    <Box my={2}>
      <Button
        color="primary"
        onClick={logoutAction}
        variant="contained"
        fullWidth
        disableElevation
      >
        Se déconnecter
      </Button>
    </Box>
  );
};
