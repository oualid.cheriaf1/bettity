import React, { useState } from "react";
import { useRouteMatch } from "react-router-dom";

import Table from "@material-ui/core/Table";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";

import { useStanding } from "../../../core/hooks/api/standing";
import { InviteLeagueModal } from "./InviteLeagueModal";

export const LeagueDetailsScene = () => {
  const {
    params: { leagueId }
  } = useRouteMatch();
  const standing = useStanding(leagueId);
  const [openModal, setOpenModal] = useState(false);
  return (
    <Box my={2} textAlign="center">
      <Paper>
        <Box p={3} m={2}>
          <Typography variant="h5">Code de la ligue :</Typography>
          <Box className="span" fontWeight={900} pb={2}>
            <Typography variant="h4" color="secondary">
              {leagueId}
            </Typography>
          </Box>
          <Button
            fullWidth
            onClick={() => setOpenModal(true)}
            variant="contained"
            color="primary"
            disableElevation
          >
            Inviter
          </Button>
        </Box>
      </Paper>
      <InviteLeagueModal
        open={openModal}
        onClose={() => setOpenModal(false)}
        leagueCode={leagueId}
      />
      {standing && (
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Nom d'utilisateur</TableCell>
                <TableCell>Points:</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {standing.map(userStanding => (
                <TableRow key={userStanding.userId}>
                  <TableCell>{userStanding.username}</TableCell>
                  <TableCell>{userStanding.numberOfPoint}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    </Box>
  );
};
