export const TEAMS = {
  759: {
    name: "Allemagne",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/b/ba/Flag_of_Germany.svg"
  },
  760: {
    name: "Espagne",
    crestUrl: "https://upload.wikimedia.org/wikipedia/en/9/9a/Flag_of_Spain.svg"
  },
  765: {
    name: "Portugal",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/5/5c/Flag_of_Portugal.svg"
  },
  770: {
    name: "Angleterre",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/en/b/be/Flag_of_England.svg"
  },
  773: {
    name: "France",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/c/c3/Flag_of_France.svg"
  },
  784: {
    name: "Italie",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/0/03/Flag_of_Italy.svg"
  },
  790: {
    name: "Ukraine",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/4/49/Flag_of_Ukraine.svg"
  },
  792: {
    name: "Suède",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/4/4c/Flag_of_Sweden.svg"
  },
  794: {
    name: "Pologne",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/1/12/Flag_of_Poland.svg"
  },
  798: {
    name: "République Tchèque",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/c/cb/Flag_of_the_Czech_Republic.svg"
  },
  799: {
    name: "Croatie",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/1/1b/Flag_of_Croatia.svg"
  },
  803: {
    name: "Turquie",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/b/b4/Flag_of_Turkey.svg"
  },
  805: {
    name: "Belgique",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/6/65/Flag_of_Belgium.svg"
  },
  808: {
    name: "Russie",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/en/f/f3/Flag_of_Russia.svg"
  },
  816: {
    name: "Autriche",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/4/41/Flag_of_Austria.svg"
  },
  788: {
    name: "Suisse",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/f/f3/Flag_of_Switzerland.svg"
  },
  833: {
    name: "Pays de Galles",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/a/a9/Flag_of_Wales_%281959%E2%80%93present%29.svg"
  },
  782: {
    name: "Danemark",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/9/9c/Flag_of_Denmark.svg"
  },
  1976: {
    name: "Finlande",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/b/bc/Flag_of_Finland.svg"
  },
  8601: {
    name: "Pays-bas",
    crestUrl:
      "https://upload.wikimedia.org/wikipedia/commons/2/20/Flag_of_the_Netherlands.svg"
  }
};
