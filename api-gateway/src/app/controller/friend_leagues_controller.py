from flask import Blueprint, g, request
import os
from src.helpers.error_handler import ErrorHandler
from src.helpers.response_normalizer import response_normalizer
from src.helpers.validators import is_defined_in_dict

from src.app.middlewares.authenticated_as import authenticated_as

from src.app.microservices.friend_leagues_service import FriendLeaguesService
from src.app.models.user import UserRole

friend_leagues_controller = Blueprint(
    "friend_leagues_controller", __name__, url_prefix="/leagues")


@friend_leagues_controller.route("/invite", methods=["POST"])
@ErrorHandler
@authenticated_as(UserRole.user)
def invite():
    """ Route to invite friends to join a league using the league code """
    invite_info = request.get_json()
    is_defined_in_dict(["league_code", "email_list"], invite_info)
    ms_response = FriendLeaguesService().invite_by_email(
        league_code=invite_info["league_code"], email_list=invite_info["email_list"])
    return response_normalizer("LEAGUE_INVITE_SENT")


@friend_leagues_controller.route("/", methods=["POST"])
@ErrorHandler
@authenticated_as(UserRole.user)
def create_friend_leagues():
    """ Route to create a league. It will generate a random league code and register the league creator into the league"""
    league_info = request.get_json()
    is_defined_in_dict("league_name", league_info)
    league_details = FriendLeaguesService().create_league(
        owner_id=g.user.id, league_name=league_info["league_name"])
    return response_normalizer("LEAGUE_CREATED", league_details)


@friend_leagues_controller.route("/join/<string:league_code>", methods=["POST"])
@ErrorHandler
@authenticated_as(UserRole.user)
def join(league_code):
    """ 
        Route user to join a league. 
        :param league_code: The code of the league to join.
        :type league_code: str, required.
    """
    ms_response = FriendLeaguesService().add_user_to_league(
        user_id=g.user.id, league_code=league_code)
    return response_normalizer("LEAGUE_JOINED", ms_response)


@friend_leagues_controller.route("/<string:league_code>", methods=["GET"])
@ErrorHandler
@authenticated_as(UserRole.user)
def get_league_ranking(league_code):
    """ 
        Route to get the league ranking.

        :param league_code: The code of the league to join.
        :type league_code: str, required.
    """
    league_ranking = FriendLeaguesService(
    ).get_league_rankings(league_code=league_code)
    return response_normalizer("LEAGUE_STANDING_RETRIEVED", league_ranking)


@friend_leagues_controller.route("/", methods=["GET"])
@ErrorHandler
@authenticated_as(UserRole.user)
def get_all_league():
    """
        Route to get all the league where the user is registered.
    """
    user_id = g.user.id
    leagues_list = FriendLeaguesService().get_user_leagues(user_id=user_id)
    return response_normalizer("LEAGUES_RETRIEVED", {"leagues_list": leagues_list})
