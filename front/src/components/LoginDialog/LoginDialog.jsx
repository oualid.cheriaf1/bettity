import React, { useContext, useEffect, useState } from "react";
import { Formik, Form, Field } from "formik";
import { TextField } from "formik-material-ui";
import { Link as RRDLink } from "react-router-dom";
import * as Yup from "yup";

import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";

import { AuthContext } from "../../contexts/AuthContext";
import { useApi } from "../../core/hooks/useApi";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export const LoginDialog = ({ open, setOpen }) => {
  const API = useApi();
  const { loginAction } = useContext(AuthContext);
  const [json, setJson] = useState();
  const [response, setResponse] = useState();
  const onClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (!open && json) {
      loginAction(json);
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open, json]);
  const sendAuthRequest = async (values, { setSubmitting }) => {
    try {
      const json = await API({
        url: "/auth/login",
        method: "POST",
        data: values
      });
      setOpen(false);
      setResponse(json);
      const { data } = json;
      setJson(data);
      setSubmitting(false);
    } catch (error) {
      setResponse(error);
    }
  };
  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={onClose}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <Formik
        initialValues={{ email: "", password: "" }}
        validationSchema={Yup.object({
          email: Yup.string().email("Veuillez entrer une adresse mail valide"),
          password: Yup.string().min(
            8,
            "Le mot de passe doit contenir au minimum 8 caractères"
          )
        })}
        onSubmit={sendAuthRequest}
      >
        <Form>
          <DialogTitle id="alert-dialog-slide-title">{"Connexion"}</DialogTitle>
          <DialogContent>
            <Field
              name="email"
              label="Email"
              type="email"
              component={TextField}
              fullWidth
            />
            <Field
              name="password"
              label="Mot de passe"
              type="password"
              component={TextField}
              margin="normal"
              fullWidth
            />
            {response && (
              <Box
                bgcolor={
                  response.status === 200 ? "success.main" : "error.main"
                }
                p={1}
                my={1}
                borderRadius={2}
              >
                {response.message}
              </Box>
            )}
          </DialogContent>
          <DialogActions>
            <Button type="submit" color="primary">
              Se connecter
            </Button>
            <Button
              onClick={onClose}
              color="primary"
              component={RRDLink}
              to="/register"
            >
              S'inscrire
            </Button>
          </DialogActions>
        </Form>
      </Formik>
    </Dialog>
  );
};
