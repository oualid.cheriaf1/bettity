import unittest

from src.helpers.error_handler import ErrorHandler


class ErrorHandlerTest(unittest.TestCase):
    def setUp(self):
        @ErrorHandler
        def raise_exceptions(*args):
            raise Exception(*args)
        self.raise_exceptions = raise_exceptions

    def test_should_return_a_function(self):
        """ Error handler is a function and should return a function """
        self.assertTrue(callable(ErrorHandler))
        @ErrorHandler
        def test1():
            pass
        self.assertTrue(callable(test1))
        self.assertTrue(test1.__name__, "test1")

    def test_should_handle_errors(self):
        """ Error handler should return a function which raises no errors """
        try:
            self.raise_exceptions('INVALID_DATA')
        except Exception:
            self.fail("Has raised an error")

    def test_should_return_flask_response(self):
        """ Error handler should return a dict object and a status code if exceptions is raised """
        json_response, status_code = self.raise_exceptions('INVALID_DATA')
        self.assertDictEqual(json_response, {
                             'status': 422, 'message': 'Les données fournies sont invalides ou incomplètes', 'data': (('email',),)})
        self.assertEqual(status_code, 422)
        json_response, status_code = self.raise_exceptions('ERROR_404')
        self.assertDictEqual(json_response, {
                             "status": 500, "message": "Une erreur interne est survenu. Veuillez réessayer plus tard.", "error": ('ERROR_404',), "stack":json_response["stack"]})
        self.assertEqual(status_code, 500)
