import json

with open("src/constant/response.json", 'r') as f:
    RESPONSES = json.load(f)


def response_normalizer(response_code, data={}):
    """
        Return the response dictionnary and the status code associated to the response code 
        and add the data to response object if data is provided.

        :param response_code: Key of the response.json file, required
        :type response_code: string

        :param data: data provided, optional, default to an empty dict
        :type data: any

        :return: Response object associated to response_code, response status code associated to response_code
        :rtype: dict, integer
    """
    response = RESPONSES[response_code]
    if data:
        response["data"] = data
    return response, response["status"]
