import requests
import os

def list_or_string_to_list(keys):
    if isinstance(keys, list):
        return keys
    else:
        return [keys]

def is_defined_in_dict(keys, dicti):
    keys_to_check = list_or_string_to_list(keys)    
    for key in keys_to_check:
        if not dicti or key not in dicti or ( key in dicti and dicti[key] == ""):
            raise Exception('INVALID_DATA')
    return True

def verify_google_recaptcha(google_recaptcha_value):
    url = "https://www.google.com/recaptcha/api/siteverify"
    response = requests.post(url, data={
                             "response": google_recaptcha_value, "secret": os.getenv('GOOGLE_RECAPTCHA_SECRET')})
    json_response = response.json()
    if not json_response["success"]:
        raise Exception("GOOGLE_RECAPTCHA_FAILURE")
