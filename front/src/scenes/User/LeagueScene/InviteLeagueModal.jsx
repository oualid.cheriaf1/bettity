import React, { useState, useContext } from "react";
import {
  Dialog,
  Box,
  Button,
  DialogTitle,
  DialogContent,
  TextField,
  IconButton,
  List,
  ListSubheader,
  ListItem,
  ListItemText,
  ListItemSecondaryAction
} from "@material-ui/core";
import { Add, Remove } from "@material-ui/icons";
import { useApi } from "../../../core/hooks/useApi";
import { SnackbarContext } from "../../../contexts/SnackbarContext";

export const InviteLeagueModal = ({ open, onClose, leagueCode }) => {
  const { open: openSnackbar } = useContext(SnackbarContext);
  const [emailList, setEmailList] = useState([]);
  const [userToAdd, setUserToAdd] = useState("");
  const API = useApi();
  const sendInviteRequest = async () => {
    const response = await API({
      method: "POST",
      url: "/leagues/invite",
      data: { email_list: emailList, league_code: leagueCode }
    });
    openSnackbar({ message: response.message, severity: "success" });
    setEmailList([]);
    setUserToAdd("");
    onClose();
  };
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Liste des invités</DialogTitle>
      <DialogContent>
        <Box display="flex" flexDirection="column">
          Veuillez ajouter l'email de vos invités
          {emailList.length > 0 && (
            <List
              subheader={<ListSubheader>Utilisateur à inviter</ListSubheader>}
            >
              {emailList.map(email => (
                <ListItem key={email}>
                  <ListItemText primary={email} />
                  <ListItemSecondaryAction>
                    <IconButton
                      onClick={() => {
                        setEmailList(emailList.filter(el => el !== email));
                      }}
                    >
                      <Remove />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
            </List>
          )}
          <Box
            display="flex"
            flexDirection="row"
            justifyContent="space-around"
            alignItems="center"
            my={2}
          >
            <TextField
              label="Adresse mail de l'invité"
              value={userToAdd}
              onChange={ev => setUserToAdd(ev.target.value)}
              variant="outlined"
            />
            <IconButton
              aria-label="add"
              onClick={() => {
                setEmailList([...emailList, userToAdd]);
                setUserToAdd("");
              }}
            >
              <Add />
            </IconButton>
          </Box>
          {/* JBR: sortir la fonction pour le onClick */}
          <Button
            variant="contained"
            disableElevation
            color="primary"
            onClick={sendInviteRequest}
          >
            Inviter
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  );
};
