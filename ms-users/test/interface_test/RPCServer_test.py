import unittest
from unittest.mock import patch
from interface.RPCServer import RPCServer
import os


class RPCServerTest(unittest.TestCase):
    """ A test case for RPCServer class and object"""

    def setUp(self):
        """ Initial set up of the test case"""
        pass

    @patch("pika.BlockingConnection")
    @patch('pika.URLParameters')
    def test_init_RPCServer(self, URLParametersMock, BlockingConnectionMock):
        """ Create a RPCServer, ie should create a channel, declare an exchange and a queue and bind the queue to the exchange"""
        fake_service_server = RPCServer("fake_service", "fake_service-rk",
                                        pika_connection_parameters=[URLParametersMock(os.getenv("RABBITMQ_URI"))])
        BlockingConnectionMock.assert_called_once_with(
            [URLParametersMock(os.getenv("RABBITMQ_URI"))])
        fake_service_server.connection.channel.assert_called_once()
        fake_service_server.channel.exchange_declare.assert_called_once_with(
            "fake_service")
        fake_service_server.channel.queue_declare.assert_called_once_with(
            "fake_service-rk")
        fake_service_server.channel.queue_bind(
            "fake_service-rk", "fake_service")

    @patch("pika.BlockingConnection")
    @patch('pika.URLParameters')
    def test_listen(self,URLParametersMock, BlockingConnectionMock):
        """ Create a RPCServer, and listen."""
        fake_service_server = RPCServer("fake_service", "fake_service-rk",
                                        pika_connection_parameters=[URLParametersMock(os.getenv("RABBITMQ_URI"))])
        fake_service_server.channel.start_consuming.side_effects = ["ledld"]
        fake_service_server.listen()
        fake_service_server.channel.basic_qos.assert_called_once_with(prefetch_count=1)
        fake_service_server.channel.basic_consume.assert_called_once_with(
            queue=fake_service_server.queue_name, on_message_callback=fake_service_server.on_request)
        fake_service_server.channel.start_consuming.assert_called_once()
    
    def test_on_request(self):
        pass