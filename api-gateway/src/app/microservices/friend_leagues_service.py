from src.helpers.RPCClient import RPCClient

class FriendLeaguesService(RPCClient):
    def __init__(self):
        super().__init__("friend_league_service", "friend_league_service-rk")
