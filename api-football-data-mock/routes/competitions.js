const router = require("express").Router();
router.get("/2018/standings", function(req, res){
    res.json({
      filters: {},
      competition: {
        id: 2018,
        area: {
          id: 2077,
          name: "Europe"
        },
        name: "European Championship",
        code: "EC",
        plan: "TIER_ONE",
        lastUpdated: "2018-08-23T12:16:01Z"
      },
      season: {
        id: 507,
        startDate: "2020-06-12",
        endDate: "2020-07-12",
        currentMatchday: 1,
        winner: null
      },
      standings: []
    });
})
router.get("/2018/teams", function(req,res){
    res.json({
      count: 20,
      filters: {},
      competition: {
        id: 2018,
        area: {
          id: 2077,
          name: "Europe"
        },
        name: "European Championship",
        code: "EC",
        plan: "TIER_ONE",
        lastUpdated: "2018-08-23T12:16:01Z"
      },
      season: {
        id: 507,
        startDate: "2020-06-12",
        endDate: "2020-07-12",
        currentMatchday: 1,
        winner: null
      },
      teams: [
        {
          id: 759,
          area: {
            id: 2088,
            name: "Germany"
          },
          name: "Germany",
          shortName: "Germany",
          tla: "GER",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/en/b/ba/Flag_of_Germany.svg",
          address:
            "Otto-Fleck-Schneise 6 / Postfach 710265 Frankfurt am Main 60492",
          phone: "+49 (69) 67880",
          website: "http://www.dfb.de",
          email: "info@dfb.de",
          founded: 1900,
          clubColors: "White / Black",
          venue: "Signal Iduna Park",
          lastUpdated: "2020-01-30T02:36:50Z"
        },
        {
          id: 760,
          area: {
            id: 2224,
            name: "Spain"
          },
          name: "Spain",
          shortName: "Spain",
          tla: "ESP",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/en/9/9a/Flag_of_Spain.svg",
          address: "Ramón y Cajal, s/n Las Rozas 28230",
          phone: "+34 (91) 4959800",
          website: "http://www.rfef.es",
          email: "rfef@rfef.es",
          founded: 1909,
          clubColors: "Red / Blue / Yellow",
          venue: "Estadio Wanda Metropolitano",
          lastUpdated: "2020-01-30T02:36:51Z"
        },
        {
          id: 765,
          area: {
            id: 2187,
            name: "Portugal"
          },
          name: "Portugal",
          shortName: "Portugal",
          tla: "POR",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/commons/5/5c/Flag_of_Portugal.svg",
          address: "Rua Alexandre Herculano, 58 Lisboa 1250-012",
          phone: "+351 (213) 252700",
          website: "http://www.fpf.pt",
          email: "info@fpf.pt",
          founded: 1914,
          clubColors: "Red / Green",
          venue: "Estádio Do Algarve",
          lastUpdated: "2020-01-30T02:36:51Z"
        },
        {
          id: 770,
          area: {
            id: 2072,
            name: "England"
          },
          name: "England",
          shortName: "England",
          tla: "ENG",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/en/b/be/Flag_of_England.svg",
          address: "Wembley Stadium, PO Box 1966 London SW1P 9EQ",
          phone: "+44 (844) 9808200",
          website: "http://www.thefa.com",
          email: "info@thefa.com",
          founded: 1863,
          clubColors: "White / Red / Navy Blue",
          venue: "Wembley Stadium",
          lastUpdated: "2020-01-30T02:36:52Z"
        },
        {
          id: 773,
          area: {
            id: 2081,
            name: "France"
          },
          name: "France",
          shortName: "France",
          tla: "FRA",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/en/c/c3/Flag_of_France.svg",
          address: "87 Boulevard de Grenelle Paris 75738",
          phone: "+33 (1) 44317300",
          website: "http://www.fff.fr",
          email: "webmaster@fff.fr",
          founded: 1919,
          clubColors: "Blue / White / Red",
          venue: "Stade de France",
          lastUpdated: "2020-01-30T02:36:53Z"
        },
        {
          id: 782,
          area: {
            id: 2065,
            name: "Denmark"
          },
          name: "Denmark",
          shortName: "Denmark",
          tla: "DEN",
          crestUrl: "",
          address: "House of Football, DBU Allé 1 Brøndby 2605",
          phone: "+45 (43) 262222",
          website: "http://www.dbu.dk",
          email: "dbu@dbu.dk",
          founded: 1889,
          clubColors: "Red / White",
          venue: "Telia Parken",
          lastUpdated: "2020-01-30T02:36:54Z"
        },
        {
          id: 784,
          area: {
            id: 2114,
            name: "Italy"
          },
          name: "Italy",
          shortName: "Italy",
          tla: "ITA",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/en/0/03/Flag_of_Italy.svg",
          address: "Via Gregorio Allegri, 14 / CP 2450 Roma 00198",
          phone: "+39 (06) 84911",
          website: "http://www.figc.it",
          email: "international@figc.it",
          founded: 1898,
          clubColors: "Blue / White",
          venue: "Stadio Olimpico",
          lastUpdated: "2020-01-30T02:36:55Z"
        },
        {
          id: 788,
          area: {
            id: 2234,
            name: "Switzerland"
          },
          name: "Switzerland",
          shortName: "Switzerland",
          tla: "SUI",
          crestUrl: null,
          address: "Worbstrasse 48 Bern 3000",
          phone: "+41 (31) 9508111",
          website: "http://www.football.ch",
          email: "sfv.asf@football.ch",
          founded: 1895,
          clubColors: "Red / White",
          venue: "Kybunpark",
          lastUpdated: "2020-01-30T02:36:55Z"
        },
        {
          id: 790,
          area: {
            id: 2253,
            name: "Ukraine"
          },
          name: "Ukraine",
          shortName: "Ukraine",
          tla: "UKR",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/commons/4/49/Flag_of_Ukraine.svg",
          address: "Provulok Laboratorniy 7-A / PO Box 55 Kyïv 01133",
          phone: "+380 (44) 5210521",
          website: "http://www.ffu.org.ua",
          email: "info@ffu.org.ua",
          founded: 1991,
          clubColors: "Yellow / Blue",
          venue: "Slavutych-Arena",
          lastUpdated: "2020-01-30T02:36:56Z"
        },
        {
          id: 792,
          area: {
            id: 2233,
            name: "Sweden"
          },
          name: "Sweden",
          shortName: "Sweden",
          tla: "SWE",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/en/4/4c/Flag_of_Sweden.svg",
          address: "Evenemangsgatan 31 / Box 1216 Solna 17123",
          phone: "+46 (8) 7350900",
          website: "http://www.svenskfotboll.se",
          email: "svff@svenskfotboll.se",
          founded: 1904,
          clubColors: "Yellow / Blue",
          venue: "Hamad bin Khalifa Stadium",
          lastUpdated: "2020-01-30T02:36:57Z"
        },
        {
          id: 794,
          area: {
            id: 2186,
            name: "Poland"
          },
          name: "Poland",
          shortName: "Poland",
          tla: "POL",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/en/1/12/Flag_of_Poland.svg",
          address: "Bitwy Warszawskiej 1920 r., 7 Warszawa 02366",
          phone: "+48 (22) 5512300",
          website: "http://www.pzpn.pl",
          email: "pzpn@pzpn.pl",
          founded: 1919,
          clubColors: "White / Red",
          venue: "Stadion Narodowy",
          lastUpdated: "2020-01-30T02:36:58Z"
        },
        {
          id: 798,
          area: {
            id: 2062,
            name: "Czech Republic"
          },
          name: "Czech Republic",
          shortName: "Czech Republic",
          tla: "CZE",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/commons/c/cb/Flag_of_the_Czech_Republic.svg",
          address: "Diskařská 2431/4 Praha 16017",
          phone: "+420 (23) 3029111",
          website: "http://www.fotbal.cz",
          email: "facr@fotbal.cz",
          founded: 1901,
          clubColors: "Red / White / Blue",
          venue: "Generali Arena",
          lastUpdated: "2020-01-30T02:36:59Z"
        },
        {
          id: 799,
          area: {
            id: 2058,
            name: "Croatia"
          },
          name: "Croatia",
          shortName: "Croatia",
          tla: "CRO",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/commons/1/1b/Flag_of_Croatia.svg",
          address: "Ulica grada Vukovara, 269 A Zagreb 10000",
          phone: "+385 (1) 2361555",
          website: "http://www.hns-cff.hr",
          email: "info@hns-cff.hr",
          founded: 1912,
          clubColors: "Red / White / Blue",
          venue: "Stadion Aldo Drosina",
          lastUpdated: "2020-01-30T02:37:01Z"
        },
        {
          id: 803,
          area: {
            id: 2247,
            name: "Turkey"
          },
          name: "Turkey",
          shortName: "Turkey",
          tla: "TUR",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/commons/b/b4/Flag_of_Turkey.svg",
          address: "Istinye Mahallesi, Darüşşafaka Caddesi, 45 İstanbul 34330",
          phone: "+90 (212) 3622222",
          website: "http://www.tff.org",
          email: "intdept@tff.org",
          founded: 1923,
          clubColors: "Red / White",
          venue: "Ali Sami Yen Stadyumu",
          lastUpdated: "2020-01-30T02:37:01Z"
        },
        {
          id: 805,
          area: {
            id: 2023,
            name: "Belgium"
          },
          name: "Belgium",
          shortName: "Belgium",
          tla: "BEL",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/commons/6/65/Flag_of_Belgium.svg",
          address: "145, avenue Houba de Strooper Brussels 1020",
          phone: "+32 (2) 4771211",
          website: "http://www.belgianfootball.be",
          email: "urbsfa.kbvb@footbel.com",
          founded: 1895,
          clubColors: "Red / Black / Yellow",
          venue: "Stade Roi Baudouin",
          lastUpdated: "2020-01-30T02:37:03Z"
        },
        {
          id: 808,
          area: {
            id: 2195,
            name: "Russia"
          },
          name: "Russia",
          shortName: "Russia",
          tla: "RUS",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/en/f/f3/Flag_of_Russia.svg",
          address: "Ulitsa Narodnaya, 7 Moskva 115172",
          phone: "+7 (495) 9261300",
          website: "http://www.rfs.ru",
          email: "info@rfs.ru",
          founded: 1912,
          clubColors: "Red / White / Blue",
          venue: "Gazprom Arena",
          lastUpdated: "2020-01-30T02:37:03Z"
        },
        {
          id: 816,
          area: {
            id: 2016,
            name: "Austria"
          },
          name: "Austria",
          shortName: "Austria",
          tla: "AUT",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/commons/4/41/Flag_of_Austria.svg",
          address:
            "Ernst-Happel-Stadion, Sektor A/F, Meiereistraße 7 Wien 1020",
          phone: "+43 (1) 727180",
          website: "http://www.oefb.at",
          email: "office@oefb.at",
          founded: 1904,
          clubColors: "Red / White / Black",
          venue: "Ernst-Happel-Stadion",
          lastUpdated: "2020-01-30T02:37:05Z"
        },
        {
          id: 833,
          area: {
            id: 2264,
            name: "Wales"
          },
          name: "Wales",
          shortName: "Wales",
          tla: "WAL",
          crestUrl:
            "https://upload.wikimedia.org/wikipedia/commons/5/59/Flag_of_Wales_2.svg",
          address: "11/12 Neptune Court, Vanguard Way Cardiff CF24 5PJ",
          phone: "+44 (2920) 435830",
          website: "http://www.faw.org.uk",
          email: "info@faw.org.uk",
          founded: 1876,
          clubColors: "Red / White",
          venue: "Cardiff City Stadium",
          lastUpdated: "2020-01-30T02:37:06Z"
        },
        {
          id: 1976,
          area: {
            id: 2080,
            name: "Finland"
          },
          name: "Finland",
          shortName: "Finland",
          tla: "FIN",
          crestUrl: null,
          address: "Urheilukatu, 5 / PO Box 191 Helsinki 00251",
          phone: "+358 (9) 742151",
          website: "http://www.palloliitto.fi",
          email: "sami.terava@palloliitto.fi",
          founded: 1907,
          clubColors: "Blue / White",
          venue: "Telia 5G -areena",
          lastUpdated: "2020-01-30T02:37:08Z"
        },
        {
          id: 8601,
          area: {
            id: 2163,
            name: "Netherlands"
          },
          name: "Netherlands",
          shortName: "Netherlands",
          tla: "NED",
          crestUrl: null,
          address: "Woudenbergseweg 56-58 / Postbus 515 Zeist 3700 AM",
          phone: "+31 (343) 499201",
          website: "http://www.knvb.nl",
          email: "concern@knvb.nl",
          founded: 1889,
          clubColors: "Orange / White / Blue",
          venue: "Johan Cruyff ArenA",
          lastUpdated: "2020-01-30T02:37:09Z"
        }
      ]
    });
})
router.get("/2018/matches", function(req, res){
    res.json({
      count: 24,
      filters: {},
      competition: {
        id: 2018,
        area: {
          id: 2077,
          name: "Europe"
        },
        name: "European Championship",
        code: "EC",
        plan: "TIER_ONE",
        lastUpdated: "2018-08-23T12:16:01Z"
      },
      matches: [
        {
          id: 285418,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-01-12T19:00:00Z",
          status: "FINISHED",
          matchday: 1,
          stage: "GROUP_STAGE",
          group: "Group A",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: "DRAW",
            duration: "REGULAR",
            fullTime: {
              homeTeam: 1,
              awayTeam: 1
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 803,
            name: "Turkey"
          },
          awayTeam: {
            id: 784,
            name: "Italy"
          },
          referees: []
        },
        {
          id: 285419,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-01-13T13:00:00Z",
          status: "FINISHED",
          matchday: 1,
          stage: "GROUP_STAGE",
          group: "Group A",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: "HOME_TEAM",
            duration: "REGULAR",
            fullTime: {
              homeTeam: 3,
              awayTeam: 2
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 833,
            name: "Wales"
          },
          awayTeam: {
            id: 788,
            name: "Switzerland"
          },
          referees: []
        },
        {
          id: 285424,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-13T16:00:00Z",
          status: "SCHEDULED",
          matchday: 1,
          stage: "GROUP_STAGE",
          group: "Group B",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 782,
            name: "Denmark"
          },
          awayTeam: {
            id: 1976,
            name: "Finland"
          },
          referees: []
        },
        {
          id: 285425,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-13T19:00:00Z",
          status: "SCHEDULED",
          matchday: 1,
          stage: "GROUP_STAGE",
          group: "Group B",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 805,
            name: "Belgium"
          },
          awayTeam: {
            id: 808,
            name: "Russia"
          },
          referees: []
        },
        {
          id: 285436,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-14T13:00:00Z",
          status: "SCHEDULED",
          matchday: 1,
          stage: "GROUP_STAGE",
          group: "Group D",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 770,
            name: "England"
          },
          awayTeam: {
            id: 799,
            name: "Croatia"
          },
          referees: []
        },
        {
          id: 285431,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-14T19:00:00Z",
          status: "SCHEDULED",
          matchday: 1,
          stage: "GROUP_STAGE",
          group: "Group C",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 8601,
            name: "Netherlands"
          },
          awayTeam: {
            id: 790,
            name: "Ukraine"
          },
          referees: []
        },
        {
          id: 285443,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-15T19:00:00Z",
          status: "SCHEDULED",
          matchday: 1,
          stage: "GROUP_STAGE",
          group: "Group E",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 760,
            name: "Spain"
          },
          awayTeam: {
            id: 792,
            name: "Sweden"
          },
          referees: []
        },
        {
          id: 285449,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-16T19:00:00Z",
          status: "SCHEDULED",
          matchday: 1,
          stage: "GROUP_STAGE",
          group: "Group F",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 773,
            name: "France"
          },
          awayTeam: {
            id: 759,
            name: "Germany"
          },
          referees: []
        },
        {
          id: 285426,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-17T13:00:00Z",
          status: "SCHEDULED",
          matchday: 2,
          stage: "GROUP_STAGE",
          group: "Group B",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 1976,
            name: "Finland"
          },
          awayTeam: {
            id: 808,
            name: "Russia"
          },
          referees: []
        },
        {
          id: 285420,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-01-17T16:00:00Z",
          status: "FINISHED",
          matchday: 2,
          stage: "GROUP_STAGE",
          group: "Group A",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: "DRAW",
            duration: "REGULAR",
            fullTime: {
              homeTeam: 1,
              awayTeam: 1
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 803,
            name: "Turkey"
          },
          awayTeam: {
            id: 833,
            name: "Wales"
          },
          referees: []
        },
        {
          id: 285421,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-01-17T19:00:00Z",
          status: "FINISHED",
          matchday: 2,
          stage: "GROUP_STAGE",
          group: "Group A",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: "AWAY_TEAM",
            duration: "REGULAR",
            fullTime: {
              homeTeam: 2,
              awayTeam: 8
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 784,
            name: "Italy"
          },
          awayTeam: {
            id: 788,
            name: "Switzerland"
          },
          referees: []
        },
        {
          id: 285427,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-18T16:00:00Z",
          status: "SCHEDULED",
          matchday: 2,
          stage: "GROUP_STAGE",
          group: "Group B",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 782,
            name: "Denmark"
          },
          awayTeam: {
            id: 805,
            name: "Belgium"
          },
          referees: []
        },
        {
          id: 285433,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-18T19:00:00Z",
          status: "SCHEDULED",
          matchday: 2,
          stage: "GROUP_STAGE",
          group: "Group C",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 8601,
            name: "Netherlands"
          },
          awayTeam: {
            id: 816,
            name: "Austria"
          },
          referees: []
        },
        {
          id: 285438,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-19T16:00:00Z",
          status: "SCHEDULED",
          matchday: 2,
          stage: "GROUP_STAGE",
          group: "Group D",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 799,
            name: "Croatia"
          },
          awayTeam: {
            id: 798,
            name: "Czech Republic"
          },
          referees: []
        },
        {
          id: 285451,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-20T16:00:00Z",
          status: "SCHEDULED",
          matchday: 2,
          stage: "GROUP_STAGE",
          group: "Group F",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 765,
            name: "Portugal"
          },
          awayTeam: {
            id: 759,
            name: "Germany"
          },
          referees: []
        },
        {
          id: 285445,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-20T19:00:00Z",
          status: "SCHEDULED",
          matchday: 2,
          stage: "GROUP_STAGE",
          group: "Group E",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 760,
            name: "Spain"
          },
          awayTeam: {
            id: 794,
            name: "Poland"
          },
          referees: []
        },
        {
          id: 285422,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-21T16:00:00Z",
          status: "SCHEDULED",
          matchday: 3,
          stage: "GROUP_STAGE",
          group: "Group A",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 784,
            name: "Italy"
          },
          awayTeam: {
            id: 833,
            name: "Wales"
          },
          referees: []
        },
        {
          id: 285423,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-21T16:00:00Z",
          status: "SCHEDULED",
          matchday: 3,
          stage: "GROUP_STAGE",
          group: "Group A",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 788,
            name: "Switzerland"
          },
          awayTeam: {
            id: 803,
            name: "Turkey"
          },
          referees: []
        },
        {
          id: 285435,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-22T16:00:00Z",
          status: "SCHEDULED",
          matchday: 3,
          stage: "GROUP_STAGE",
          group: "Group C",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 790,
            name: "Ukraine"
          },
          awayTeam: {
            id: 816,
            name: "Austria"
          },
          referees: []
        },
        {
          id: 285428,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-22T19:00:00Z",
          status: "SCHEDULED",
          matchday: 3,
          stage: "GROUP_STAGE",
          group: "Group B",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 808,
            name: "Russia"
          },
          awayTeam: {
            id: 782,
            name: "Denmark"
          },
          referees: []
        },
        {
          id: 285429,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-22T19:00:00Z",
          status: "SCHEDULED",
          matchday: 3,
          stage: "GROUP_STAGE",
          group: "Group B",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 1976,
            name: "Finland"
          },
          awayTeam: {
            id: 805,
            name: "Belgium"
          },
          referees: []
        },
        {
          id: 285441,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-23T19:00:00Z",
          status: "SCHEDULED",
          matchday: 3,
          stage: "GROUP_STAGE",
          group: "Group D",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 798,
            name: "Czech Republic"
          },
          awayTeam: {
            id: 770,
            name: "England"
          },
          referees: []
        },
        {
          id: 285447,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-24T16:00:00Z",
          status: "SCHEDULED",
          matchday: 3,
          stage: "GROUP_STAGE",
          group: "Group E",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 792,
            name: "Sweden"
          },
          awayTeam: {
            id: 794,
            name: "Poland"
          },
          referees: []
        },
        {
          id: 285453,
          season: {
            id: 507,
            startDate: "2020-06-12",
            endDate: "2020-07-12",
            currentMatchday: 1
          },
          utcDate: "2020-06-26T19:00:00Z",
          status: "SCHEDULED",
          matchday: 3,
          stage: "GROUP_STAGE",
          group: "Group F",
          lastUpdated: "2019-12-01T00:34:14Z",
          score: {
            winner: null,
            duration: "REGULAR",
            fullTime: {
              homeTeam: null,
              awayTeam: null
            },
            halfTime: {
              homeTeam: null,
              awayTeam: null
            },
            extraTime: {
              homeTeam: null,
              awayTeam: null
            },
            penalties: {
              homeTeam: null,
              awayTeam: null
            }
          },
          homeTeam: {
            id: 765,
            name: "Portugal"
          },
          awayTeam: {
            id: 773,
            name: "France"
          },
          referees: []
        }
      ]
    });
})
router.get("/2018", function(req, res){
    res.json({
      id: 2018,
      area: {
        id: 2077,
        name: "Europe"
      },
      name: "European Championship",
      code: "EC",
      emblemUrl: null,
      plan: "TIER_ONE",
      currentSeason: {
        id: 507,
        startDate: "2020-06-12",
        endDate: "2020-07-12",
        currentMatchday: 1,
        winner: null
      },
      seasons: [
        {
          id: 507,
          startDate: "2020-06-12",
          endDate: "2020-07-12",
          currentMatchday: 1,
          winner: null
        },
        {
          id: 20,
          startDate: "2016-06-10",
          endDate: "2016-07-10",
          currentMatchday: 3,
          winner: {
            id: 765,
            name: "Portugal",
            shortName: "Portugal",
            tla: "POR",
            crestUrl:
              "https://upload.wikimedia.org/wikipedia/commons/5/5c/Flag_of_Portugal.svg"
          }
        }
      ],
      lastUpdated: "2018-08-23T12:16:01Z"
    });
})
module.exports = router;
